# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: haste_hustle
# Generation Time: 2018-10-12 21:08:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table wp_commentmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_commentmeta`;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;



# Dump of table wp_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_comments`;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`)
VALUES
	(1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2018-09-12 14:37:43','2018-09-12 18:37:43','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'post-trashed','','',0,0);

/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_links`;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;



# Dump of table wp_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_options`;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`)
VALUES
	(1,'siteurl','http://local.hasteandhustle.com','yes'),
	(2,'home','http://local.hasteandhustle.com','yes'),
	(3,'blogname','Haste &amp; Hustle','yes'),
	(4,'blogdescription','Just another WordPress site','yes'),
	(5,'users_can_register','0','yes'),
	(6,'admin_email','omari@kleurvision.com','yes'),
	(7,'start_of_week','1','yes'),
	(8,'use_balanceTags','0','yes'),
	(9,'use_smilies','1','yes'),
	(10,'require_name_email','1','yes'),
	(11,'comments_notify','1','yes'),
	(12,'posts_per_rss','10','yes'),
	(13,'rss_use_excerpt','0','yes'),
	(14,'mailserver_url','mail.example.com','yes'),
	(15,'mailserver_login','login@example.com','yes'),
	(16,'mailserver_pass','password','yes'),
	(17,'mailserver_port','110','yes'),
	(18,'default_category','1','yes'),
	(19,'default_comment_status','open','yes'),
	(20,'default_ping_status','open','yes'),
	(21,'default_pingback_flag','1','yes'),
	(22,'posts_per_page','10','yes'),
	(23,'date_format','F j, Y','yes'),
	(24,'time_format','g:i a','yes'),
	(25,'links_updated_date_format','F j, Y g:i a','yes'),
	(26,'comment_moderation','0','yes'),
	(27,'moderation_notify','1','yes'),
	(28,'permalink_structure','/%postname%/','yes'),
	(29,'rewrite_rules','a:88:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=5&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes'),
	(30,'hack_file','0','yes'),
	(31,'blog_charset','UTF-8','yes'),
	(32,'moderation_keys','','no'),
	(33,'active_plugins','a:3:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:37:\"tinymce-advanced/tinymce-advanced.php\";i:2;s:33:\"wp-user-avatar/wp-user-avatar.php\";}','yes'),
	(34,'category_base','','yes'),
	(35,'ping_sites','http://rpc.pingomatic.com/','yes'),
	(36,'comment_max_links','2','yes'),
	(37,'gmt_offset','0','yes'),
	(38,'default_email_category','1','yes'),
	(39,'recently_edited','','no'),
	(40,'template','understrap','yes'),
	(41,'stylesheet','hastehustle','yes'),
	(42,'comment_whitelist','1','yes'),
	(43,'blacklist_keys','','no'),
	(44,'comment_registration','0','yes'),
	(45,'html_type','text/html','yes'),
	(46,'use_trackback','0','yes'),
	(47,'default_role','subscriber','yes'),
	(48,'db_version','38590','yes'),
	(49,'uploads_use_yearmonth_folders','1','yes'),
	(50,'upload_path','','yes'),
	(51,'blog_public','1','yes'),
	(52,'default_link_category','2','yes'),
	(53,'show_on_front','page','yes'),
	(54,'tag_base','','yes'),
	(55,'show_avatars','1','yes'),
	(56,'avatar_rating','G','yes'),
	(57,'upload_url_path','','yes'),
	(58,'thumbnail_size_w','150','yes'),
	(59,'thumbnail_size_h','150','yes'),
	(60,'thumbnail_crop','1','yes'),
	(61,'medium_size_w','300','yes'),
	(62,'medium_size_h','300','yes'),
	(63,'avatar_default','mystery','yes'),
	(64,'large_size_w','1024','yes'),
	(65,'large_size_h','1024','yes'),
	(66,'image_default_link_type','none','yes'),
	(67,'image_default_size','','yes'),
	(68,'image_default_align','','yes'),
	(69,'close_comments_for_old_posts','0','yes'),
	(70,'close_comments_days_old','14','yes'),
	(71,'thread_comments','1','yes'),
	(72,'thread_comments_depth','5','yes'),
	(73,'page_comments','0','yes'),
	(74,'comments_per_page','50','yes'),
	(75,'default_comments_page','newest','yes'),
	(76,'comment_order','asc','yes'),
	(77,'sticky_posts','a:0:{}','yes'),
	(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(79,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
	(80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
	(81,'uninstall_plugins','a:0:{}','no'),
	(82,'timezone_string','America/Toronto','yes'),
	(83,'page_for_posts','0','yes'),
	(84,'page_on_front','5','yes'),
	(85,'default_post_format','0','yes'),
	(86,'link_manager_enabled','0','yes'),
	(87,'finished_splitting_shared_terms','1','yes'),
	(88,'site_icon','0','yes'),
	(89,'medium_large_size_w','768','yes'),
	(90,'medium_large_size_h','0','yes'),
	(91,'wp_page_for_privacy_policy','3','yes'),
	(92,'show_comments_cookies_opt_in','0','yes'),
	(93,'initial_db_version','38590','yes'),
	(94,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),
	(95,'fresh_site','0','yes'),
	(96,'WPLANG','en_CA','yes'),
	(97,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(98,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(99,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(100,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(101,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(102,'sidebars_widgets','a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"herocanvas\";a:0:{}s:10:\"statichero\";a:0:{}s:10:\"footerfull\";a:0:{}s:13:\"array_version\";i:3;}','yes'),
	(103,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(104,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(105,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(106,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(107,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(108,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(109,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(110,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(111,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(112,'cron','a:5:{i:1539380264;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1539412664;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1539456033;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539461159;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),
	(113,'theme_mods_twentyseventeen','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1536778242;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}','yes'),
	(141,'can_compress_scripts','1','no'),
	(142,'current_theme','UnderStrap Child','yes'),
	(143,'theme_mods_understrap','a:6:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1536779585;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"herocanvas\";a:0:{}s:10:\"statichero\";a:0:{}s:10:\"footerfull\";a:0:{}}}}','yes'),
	(144,'theme_switched','','yes'),
	(146,'theme_mods_hastehustle','a:7:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:4:\"none\";s:25:\"understrap_container_type\";s:9:\"container\";s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";s:0:\"\";}','yes'),
	(173,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),
	(281,'recently_activated','a:0:{}','yes'),
	(282,'widget_wp_user_avatar_profile','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(283,'avatar_default_wp_user_avatar','','yes'),
	(284,'wp_user_avatar_allow_upload','0','yes'),
	(285,'wp_user_avatar_disable_gravatar','0','yes'),
	(286,'wp_user_avatar_edit_avatar','1','yes'),
	(287,'wp_user_avatar_resize_crop','0','yes'),
	(288,'wp_user_avatar_resize_h','96','yes'),
	(289,'wp_user_avatar_resize_upload','0','yes'),
	(290,'wp_user_avatar_resize_w','96','yes'),
	(291,'wp_user_avatar_tinymce','1','yes'),
	(292,'wp_user_avatar_upload_size_limit','0','yes'),
	(293,'wp_user_avatar_default_avatar_updated','1','yes'),
	(294,'wp_user_avatar_users_updated','1','yes'),
	(295,'wp_user_avatar_media_updated','1','yes'),
	(426,'category_children','a:1:{i:11;a:4:{i:0;i:3;i:1;i:4;i:2;i:5;i:3;i:6;}}','yes'),
	(459,'acf_version','5.7.7','yes'),
	(462,'tadv_settings','a:6:{s:9:\"toolbar_1\";s:106:\"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,undo,redo\";s:9:\"toolbar_2\";s:103:\"fontselect,fontsizeselect,outdent,indent,pastetext,removeformat,charmap,wp_more,forecolor,table,wp_help\";s:9:\"toolbar_3\";s:0:\"\";s:9:\"toolbar_4\";s:0:\"\";s:7:\"options\";s:15:\"menubar,advlist\";s:7:\"plugins\";s:13:\"table,advlist\";}','yes'),
	(463,'tadv_admin_settings','a:1:{s:16:\"disabled_editors\";s:0:\"\";}','yes'),
	(464,'tadv_version','4000','yes'),
	(511,'_transient_is_multi_author','0','yes'),
	(512,'_transient_understrap_categories','2','yes'),
	(525,'_site_transient_timeout_theme_roots','1539371347','no'),
	(526,'_site_transient_theme_roots','a:5:{s:11:\"hastehustle\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:10:\"understrap\";s:7:\"/themes\";}','no'),
	(527,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/en_CA/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_CA\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/en_CA/wordpress-4.9.8.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1539369547;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}','no'),
	(528,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1539369547;s:7:\"checked\";a:5:{s:11:\"hastehustle\";s:5:\"0.5.3\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";s:10:\"understrap\";s:5:\"0.8.7\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),
	(529,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1539369548;s:7:\"checked\";a:5:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.7.7\";s:19:\"akismet/akismet.php\";s:5:\"4.0.8\";s:9:\"hello.php\";s:3:\"1.7\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:5:\"4.8.0\";s:33:\"wp-user-avatar/wp-user-avatar.php\";s:5:\"2.1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:16:\"tinymce-advanced\";s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.8.0\";s:7:\"updated\";s:19:\"2017-04-23 23:59:31\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/translation/plugin/tinymce-advanced/4.8.0/en_CA.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:5:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.7.7\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.7.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"tinymce-advanced/tinymce-advanced.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/tinymce-advanced\";s:4:\"slug\";s:16:\"tinymce-advanced\";s:6:\"plugin\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:11:\"new_version\";s:5:\"4.8.0\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/tinymce-advanced/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/tinymce-advanced.4.8.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-256x256.png?rev=971511\";s:2:\"1x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-128x128.png?rev=971511\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/tinymce-advanced/assets/banner-772x250.png?rev=894078\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"wp-user-avatar/wp-user-avatar.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-user-avatar\";s:4:\"slug\";s:14:\"wp-user-avatar\";s:6:\"plugin\";s:33:\"wp-user-avatar/wp-user-avatar.php\";s:11:\"new_version\";s:5:\"2.1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-user-avatar/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/wp-user-avatar.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-user-avatar/assets/icon-256x256.png?rev=1755722\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-user-avatar/assets/icon-128x128.png?rev=1755722\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-user-avatar/assets/banner-772x250.png?rev=882713\";}s:11:\"banners_rtl\";a:0:{}}}}','no');

/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_postmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_postmeta`;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`)
VALUES
	(1,2,'_wp_page_template','default'),
	(2,3,'_wp_page_template','default'),
	(3,5,'_edit_last','1'),
	(4,5,'_edit_lock','1539181888:1'),
	(5,5,'_wp_page_template','front-page.php'),
	(6,7,'_wp_trash_meta_status','publish'),
	(7,7,'_wp_trash_meta_time','1536782828'),
	(8,8,'_wp_trash_meta_status','publish'),
	(9,8,'_wp_trash_meta_time','1536782850'),
	(12,10,'_wp_trash_meta_status','publish'),
	(13,10,'_wp_trash_meta_time','1536783095'),
	(14,11,'_wp_trash_meta_status','publish'),
	(15,11,'_wp_trash_meta_time','1536783612'),
	(16,12,'_wp_trash_meta_status','publish'),
	(17,12,'_wp_trash_meta_time','1536783855'),
	(18,14,'_wp_trash_meta_status','publish'),
	(19,14,'_wp_trash_meta_time','1536784179'),
	(20,15,'_wp_trash_meta_status','publish'),
	(21,15,'_wp_trash_meta_time','1536784196'),
	(24,17,'_wp_trash_meta_status','publish'),
	(25,17,'_wp_trash_meta_time','1536784393'),
	(35,19,'_menu_item_type','post_type'),
	(36,19,'_menu_item_menu_item_parent','0'),
	(37,19,'_menu_item_object_id','5'),
	(38,19,'_menu_item_object','page'),
	(39,19,'_menu_item_target',''),
	(40,19,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(41,19,'_menu_item_xfn',''),
	(42,19,'_menu_item_url',''),
	(43,19,'_menu_item_orphaned','1536784976'),
	(44,20,'_menu_item_type','post_type'),
	(45,20,'_menu_item_menu_item_parent','0'),
	(46,20,'_menu_item_object_id','2'),
	(47,20,'_menu_item_object','page'),
	(48,20,'_menu_item_target',''),
	(49,20,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(50,20,'_menu_item_xfn',''),
	(51,20,'_menu_item_url',''),
	(52,20,'_menu_item_orphaned','1536784976'),
	(53,21,'_edit_last','1'),
	(54,21,'_wp_page_template','default'),
	(55,21,'_edit_lock','1536784995:1'),
	(56,23,'_edit_last','1'),
	(57,23,'_wp_page_template','default'),
	(58,23,'_edit_lock','1536785012:1'),
	(59,25,'_edit_last','1'),
	(60,25,'_wp_page_template','default'),
	(61,25,'_edit_lock','1536785351:1'),
	(62,27,'_edit_last','1'),
	(63,27,'_wp_page_template','default'),
	(64,27,'_edit_lock','1536785367:1'),
	(65,29,'_edit_last','1'),
	(66,29,'_wp_page_template','default'),
	(67,29,'_edit_lock','1536786241:1'),
	(121,37,'_wp_trash_meta_status','publish'),
	(122,37,'_wp_trash_meta_time','1537209589'),
	(123,38,'_wp_attached_file','2018/09/hh_logo.png'),
	(124,38,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:210;s:6:\"height\";i:257;s:4:\"file\";s:19:\"2018/09/hh_logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"hh_logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(125,39,'_menu_item_type','custom'),
	(126,39,'_menu_item_menu_item_parent','0'),
	(127,39,'_menu_item_object_id','39'),
	(128,39,'_menu_item_object','custom'),
	(129,39,'_menu_item_target',''),
	(130,39,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(131,39,'_menu_item_xfn',''),
	(132,39,'_menu_item_url','#'),
	(133,39,'_menu_item_orphaned','1537223637'),
	(178,45,'_edit_last','1'),
	(179,45,'_edit_lock','1537292276:1'),
	(180,46,'_wp_attached_file','2018/09/sample-hero@2x.jpg'),
	(181,46,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2160;s:6:\"height\";i:1240;s:4:\"file\";s:26:\"2018/09/sample-hero@2x.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"sample-hero@2x-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"sample-hero@2x-300x172.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"sample-hero@2x-768x441.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:441;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"sample-hero@2x-1024x588.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:588;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(182,45,'_thumbnail_id','46'),
	(185,48,'_edit_last','1'),
	(186,48,'_edit_lock','1537565108:1'),
	(187,49,'_wp_attached_file','2018/09/secondary-hero@2x.jpg'),
	(188,49,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1040;s:6:\"height\";i:1240;s:4:\"file\";s:29:\"2018/09/secondary-hero@2x.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"secondary-hero@2x-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"secondary-hero@2x-252x300.jpg\";s:5:\"width\";i:252;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"secondary-hero@2x-768x916.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:916;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"secondary-hero@2x-859x1024.jpg\";s:5:\"width\";i:859;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(189,48,'_thumbnail_id','49'),
	(190,51,'_wp_attached_file','2018/09/ex-prof-pic.jpg'),
	(191,51,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:1366;s:4:\"file\";s:23:\"2018/09/ex-prof-pic.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"ex-prof-pic-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"ex-prof-pic-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"ex-prof-pic-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"ex-prof-pic-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(195,52,'_wp_attached_file','2018/09/lastest-new-bkg.jpg'),
	(196,52,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2178;s:6:\"height\";i:174;s:4:\"file\";s:27:\"2018/09/lastest-new-bkg.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"lastest-new-bkg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"lastest-new-bkg-300x24.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:24;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"lastest-new-bkg-768x61.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:61;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"lastest-new-bkg-1024x82.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:82;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(197,53,'_edit_last','1'),
	(198,53,'_edit_lock','1539295806:1'),
	(199,54,'_wp_attached_file','2018/09/Manu-Goswami.jpg'),
	(200,54,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2018/09/Manu-Goswami.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"Manu-Goswami-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"Manu-Goswami-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"Manu-Goswami-768x307.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"Manu-Goswami-1024x410.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:410;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(201,53,'_thumbnail_id','54'),
	(204,1,'_wp_trash_meta_status','publish'),
	(205,1,'_wp_trash_meta_time','1537570009'),
	(206,1,'_wp_desired_post_slug','hello-world'),
	(207,1,'_wp_trash_meta_comments_status','a:1:{i:1;s:1:\"1\";}'),
	(208,58,'_edit_last','1'),
	(209,58,'_edit_lock','1539295615:1'),
	(210,59,'_wp_attached_file','2018/09/generation_now_interac.png'),
	(211,59,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:480;s:4:\"file\";s:34:\"2018/09/generation_now_interac.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"generation_now_interac-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"generation_now_interac-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"generation_now_interac-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"generation_now_interac-1024x410.png\";s:5:\"width\";i:1024;s:6:\"height\";i:410;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(212,58,'_thumbnail_id','59'),
	(213,61,'_edit_last','1'),
	(214,61,'_wp_page_template','default'),
	(215,61,'_edit_lock','1537824137:1'),
	(216,63,'_edit_last','1'),
	(217,63,'_wp_page_template','default'),
	(218,63,'_edit_lock','1537824145:1'),
	(219,65,'_edit_last','1'),
	(220,65,'_wp_page_template','default'),
	(221,65,'_edit_lock','1537824168:1'),
	(222,67,'_menu_item_type','post_type'),
	(223,67,'_menu_item_menu_item_parent','0'),
	(224,67,'_menu_item_object_id','65'),
	(225,67,'_menu_item_object','page'),
	(226,67,'_menu_item_target',''),
	(227,67,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(228,67,'_menu_item_xfn',''),
	(229,67,'_menu_item_url',''),
	(240,69,'_menu_item_type','post_type'),
	(241,69,'_menu_item_menu_item_parent','0'),
	(242,69,'_menu_item_object_id','61'),
	(243,69,'_menu_item_object','page'),
	(244,69,'_menu_item_target',''),
	(245,69,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(246,69,'_menu_item_xfn',''),
	(247,69,'_menu_item_url',''),
	(294,75,'_menu_item_type','custom'),
	(295,75,'_menu_item_menu_item_parent','0'),
	(296,75,'_menu_item_object_id','75'),
	(297,75,'_menu_item_object','custom'),
	(298,75,'_menu_item_target',''),
	(299,75,'_menu_item_classes','a:1:{i:0;s:11:\"social-icon\";}'),
	(300,75,'_menu_item_xfn',''),
	(301,75,'_menu_item_url','#'),
	(321,78,'_menu_item_type','custom'),
	(322,78,'_menu_item_menu_item_parent','0'),
	(323,78,'_menu_item_object_id','78'),
	(324,78,'_menu_item_object','custom'),
	(325,78,'_menu_item_target',''),
	(326,78,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(327,78,'_menu_item_xfn',''),
	(328,78,'_menu_item_url','/category/resources/'),
	(329,80,'_edit_last','1'),
	(330,80,'_wp_page_template','default'),
	(331,80,'_edit_lock','1539092544:1'),
	(332,82,'_edit_last','1'),
	(333,82,'_wp_page_template','default'),
	(334,82,'_edit_lock','1539092555:1'),
	(335,84,'_edit_last','1'),
	(336,84,'_wp_page_template','default'),
	(337,84,'_edit_lock','1539092714:1'),
	(338,86,'_menu_item_type','post_type'),
	(339,86,'_menu_item_menu_item_parent','0'),
	(340,86,'_menu_item_object_id','5'),
	(341,86,'_menu_item_object','page'),
	(342,86,'_menu_item_target',''),
	(343,86,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(344,86,'_menu_item_xfn',''),
	(345,86,'_menu_item_url',''),
	(346,86,'_menu_item_orphaned','1539092881'),
	(347,87,'_menu_item_type','post_type'),
	(348,87,'_menu_item_menu_item_parent','0'),
	(349,87,'_menu_item_object_id','84'),
	(350,87,'_menu_item_object','page'),
	(351,87,'_menu_item_target',''),
	(352,87,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(353,87,'_menu_item_xfn',''),
	(354,87,'_menu_item_url',''),
	(355,87,'_menu_item_orphaned','1539093208'),
	(356,88,'_menu_item_type','post_type'),
	(357,88,'_menu_item_menu_item_parent','0'),
	(358,88,'_menu_item_object_id','82'),
	(359,88,'_menu_item_object','page'),
	(360,88,'_menu_item_target',''),
	(361,88,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(362,88,'_menu_item_xfn',''),
	(363,88,'_menu_item_url',''),
	(364,88,'_menu_item_orphaned','1539093208'),
	(365,89,'_menu_item_type','post_type'),
	(366,89,'_menu_item_menu_item_parent','0'),
	(367,89,'_menu_item_object_id','80'),
	(368,89,'_menu_item_object','page'),
	(369,89,'_menu_item_target',''),
	(370,89,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(371,89,'_menu_item_xfn',''),
	(372,89,'_menu_item_url',''),
	(373,89,'_menu_item_orphaned','1539093208'),
	(374,90,'_edit_last','1'),
	(375,90,'_edit_lock','1539093103:1'),
	(376,90,'_wp_page_template','default'),
	(377,92,'_menu_item_type','post_type'),
	(378,92,'_menu_item_menu_item_parent','0'),
	(379,92,'_menu_item_object_id','90'),
	(380,92,'_menu_item_object','page'),
	(381,92,'_menu_item_target',''),
	(382,92,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(383,92,'_menu_item_xfn',''),
	(384,92,'_menu_item_url',''),
	(386,93,'_menu_item_type','post_type'),
	(387,93,'_menu_item_menu_item_parent','0'),
	(388,93,'_menu_item_object_id','84'),
	(389,93,'_menu_item_object','page'),
	(390,93,'_menu_item_target',''),
	(391,93,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(392,93,'_menu_item_xfn',''),
	(393,93,'_menu_item_url',''),
	(395,94,'_menu_item_type','post_type'),
	(396,94,'_menu_item_menu_item_parent','0'),
	(397,94,'_menu_item_object_id','82'),
	(398,94,'_menu_item_object','page'),
	(399,94,'_menu_item_target',''),
	(400,94,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(401,94,'_menu_item_xfn',''),
	(402,94,'_menu_item_url',''),
	(404,95,'_menu_item_type','post_type'),
	(405,95,'_menu_item_menu_item_parent','0'),
	(406,95,'_menu_item_object_id','80'),
	(407,95,'_menu_item_object','page'),
	(408,95,'_menu_item_target',''),
	(409,95,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(410,95,'_menu_item_xfn',''),
	(411,95,'_menu_item_url',''),
	(413,63,'_wp_trash_meta_status','publish'),
	(414,63,'_wp_trash_meta_time','1539093272'),
	(415,63,'_wp_desired_post_slug','contact'),
	(416,96,'_menu_item_type','post_type'),
	(417,96,'_menu_item_menu_item_parent','0'),
	(418,96,'_menu_item_object_id','5'),
	(419,96,'_menu_item_object','page'),
	(420,96,'_menu_item_target',''),
	(421,96,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(422,96,'_menu_item_xfn',''),
	(423,96,'_menu_item_url',''),
	(425,97,'_edit_last','1'),
	(426,97,'_wp_page_template','default'),
	(427,97,'_edit_lock','1539093250:1'),
	(428,99,'_menu_item_type','post_type'),
	(429,99,'_menu_item_menu_item_parent','0'),
	(430,99,'_menu_item_object_id','97'),
	(431,99,'_menu_item_object','page'),
	(432,99,'_menu_item_target',''),
	(433,99,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(434,99,'_menu_item_xfn',''),
	(435,99,'_menu_item_url',''),
	(437,100,'_edit_last','1'),
	(438,100,'_wp_page_template','default'),
	(439,100,'_edit_lock','1539093292:1'),
	(440,102,'_wp_trash_meta_status','publish'),
	(441,102,'_wp_trash_meta_time','1539093957'),
	(442,103,'_wp_trash_meta_status','publish'),
	(443,103,'_wp_trash_meta_time','1539093972'),
	(444,105,'_edit_last','1'),
	(445,105,'_edit_lock','1539292736:1'),
	(448,58,'subtitle','With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one of the premier conferences for entrepreneurs to attend.'),
	(449,58,'_subtitle','field_5bbe1264a349b'),
	(450,58,'first_content','Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.\r\n\r\nYou will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.\r\n<h3><b>Networking Opportunities</b></h3>\r\nThrough Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.'),
	(451,58,'_first_content','field_5bbe11866beeb'),
	(452,58,'break_callout_choice','1'),
	(453,58,'_break_callout_choice','field_5bbe145726aaf'),
	(454,58,'second_content','<h3><b>Industry Leading Speakers</b></h3>\r\nThe best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.\r\n\r\nPeople like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\nAll these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.\r\n\r\nGet your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!'),
	(455,58,'_second_content','field_5bbe1536f7394'),
	(456,58,'block_quote',''),
	(457,58,'_block_quote','field_5bbe194f2bc82'),
	(458,113,'subtitle','With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one of the premier conferences for entrepreneurs to attend.'),
	(459,113,'_subtitle','field_5bbe1264a349b'),
	(460,113,'first_content','<p>Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.</p>\r\n<p>You will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.</p>\r\n<h3><b>Networking Opportunities</b></h3>\r\n<p>Through Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.</p>\r\n<p>For two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.</p>\r\n<h3><b>Industry Leading Speakers</b></h3>\r\n<p>The best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.</p>\r\n<p>People like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.</p>\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\n<p>All these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.</p>\r\n<p>Get your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!</p>'),
	(461,113,'_first_content','field_5bbe11866beeb'),
	(462,113,'break_callout_choice','0'),
	(463,113,'_break_callout_choice','field_5bbe145726aaf'),
	(464,113,'second_content',''),
	(465,113,'_second_content','field_5bbe1536f7394'),
	(466,113,'block_quote',''),
	(467,113,'_block_quote','field_5bbe194f2bc82'),
	(469,51,'_wp_attachment_wp_user_avatar','1'),
	(470,114,'_wp_attached_file','2018/09/callout-bkg.jpg'),
	(471,114,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1911;s:6:\"height\";i:620;s:4:\"file\";s:23:\"2018/09/callout-bkg.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"callout-bkg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"callout-bkg-300x97.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:97;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"callout-bkg-768x249.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:249;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"callout-bkg-1024x332.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:332;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(474,58,'break_callout_img','114'),
	(475,58,'_break_callout_img','field_5bbe129826aad'),
	(476,58,'break_callout_content','For two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.'),
	(477,58,'_break_callout_content','field_5bbe139626aae'),
	(478,115,'subtitle','With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one of the premier conferences for entrepreneurs to attend.'),
	(479,115,'_subtitle','field_5bbe1264a349b'),
	(480,115,'first_content','<p>Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.</p>\r\n<p>You will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.</p>\r\n<h3><b>Networking Opportunities</b></h3>\r\n<p>Through Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>'),
	(481,115,'_first_content','field_5bbe11866beeb'),
	(482,115,'break_callout_choice','1'),
	(483,115,'_break_callout_choice','field_5bbe145726aaf'),
	(484,115,'second_content','<h3><b>Industry Leading Speakers</b></h3>\r\n<p>The best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.</p>\r\n<p>People like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.</p>\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\n<p>All these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.</p>\r\n<p>Get your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!</p>'),
	(485,115,'_second_content','field_5bbe1536f7394'),
	(486,115,'block_quote',''),
	(487,115,'_block_quote','field_5bbe194f2bc82'),
	(488,115,'break_callout_img','114'),
	(489,115,'_break_callout_img','field_5bbe129826aad'),
	(490,115,'break_callout_content','<p>For two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.</p>'),
	(491,115,'_break_callout_content','field_5bbe139626aae'),
	(494,116,'subtitle','With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one of the premier conferences for entrepreneurs to attend.'),
	(495,116,'_subtitle','field_5bbe1264a349b'),
	(496,116,'first_content','<p>Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.</p>\r\n<p>You will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.</p>\r\n<h3><b>Networking Opportunities</b></h3>\r\n<p>Through Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.</p>'),
	(497,116,'_first_content','field_5bbe11866beeb'),
	(498,116,'break_callout_choice','1'),
	(499,116,'_break_callout_choice','field_5bbe145726aaf'),
	(500,116,'second_content','<h3><b>Industry Leading Speakers</b></h3>\r\n<p>The best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.</p>\r\n<p>People like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.</p>\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\n<p>All these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.</p>\r\n<p>Get your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!</p>'),
	(501,116,'_second_content','field_5bbe1536f7394'),
	(502,116,'block_quote',''),
	(503,116,'_block_quote','field_5bbe194f2bc82'),
	(504,116,'break_callout_img','114'),
	(505,116,'_break_callout_img','field_5bbe129826aad'),
	(506,116,'break_callout_content','<p>For two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.</p>'),
	(507,116,'_break_callout_content','field_5bbe139626aae'),
	(510,117,'subtitle','With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one of the premier conferences for entrepreneurs to attend.'),
	(511,117,'_subtitle','field_5bbe1264a349b'),
	(512,117,'first_content','Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.\r\n\r\nYou will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.\r\n<h3><b>Networking Opportunities</b></h3>\r\nThrough Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.'),
	(513,117,'_first_content','field_5bbe11866beeb'),
	(514,117,'break_callout_choice','1'),
	(515,117,'_break_callout_choice','field_5bbe145726aaf'),
	(516,117,'second_content','<h3><b>Industry Leading Speakers</b></h3>\r\nThe best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.\r\n\r\nPeople like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\nAll these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.\r\n\r\nGet your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!'),
	(517,117,'_second_content','field_5bbe1536f7394'),
	(518,117,'block_quote',''),
	(519,117,'_block_quote','field_5bbe194f2bc82'),
	(520,117,'break_callout_img','114'),
	(521,117,'_break_callout_img','field_5bbe129826aad'),
	(522,117,'break_callout_content','For two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.'),
	(523,117,'_break_callout_content','field_5bbe139626aae'),
	(526,118,'_menu_item_type','post_type'),
	(527,118,'_menu_item_menu_item_parent','0'),
	(528,118,'_menu_item_object_id','80'),
	(529,118,'_menu_item_object','page'),
	(530,118,'_menu_item_target',''),
	(531,118,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(532,118,'_menu_item_xfn',''),
	(533,118,'_menu_item_url','');

/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_posts`;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`)
VALUES
	(1,1,'2018-09-12 14:37:43','2018-09-12 18:37:43','Welcome to WordPress. This is your first post. Edit or delete it, then start writing!','Hello world!','','trash','open','open','','hello-world__trashed','','','2018-09-21 18:46:49','2018-09-21 22:46:49','',0,'http://local.hasteandhustle.com/?p=1',0,'post','',1),
	(2,1,'2018-09-12 14:37:43','2018-09-12 18:37:43','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Halifax, have a great dog named Jack, and I like . (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://local.hasteandhustle.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','publish','closed','open','','sample-page','','','2018-09-12 14:37:43','2018-09-12 18:37:43','',0,'http://local.hasteandhustle.com/?page_id=2',0,'page','',0),
	(3,1,'2018-09-12 14:37:43','2018-09-12 18:37:43','<h2>Who we are</h2><p>Our website address is: http://local.hasteandhustle.com.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>','Privacy Policy','','draft','closed','open','','privacy-policy','','','2018-09-12 14:37:43','2018-09-12 18:37:43','',0,'http://local.hasteandhustle.com/?page_id=3',0,'page','',0),
	(5,1,'2018-09-12 16:06:34','2018-09-12 20:06:34','','Home','','publish','closed','closed','','home','','','2018-09-21 18:41:29','2018-09-21 22:41:29','',0,'http://local.hasteandhustle.com/?page_id=5',0,'page','',0),
	(6,1,'2018-09-12 16:06:34','2018-09-12 20:06:34','','Home','','inherit','closed','closed','','5-revision-v1','','','2018-09-12 16:06:34','2018-09-12 20:06:34','',5,'http://local.hasteandhustle.com/2018/09/12/5-revision-v1/',0,'revision','',0),
	(7,1,'2018-09-12 16:07:08','2018-09-12 20:07:08','{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:07:08\"\n    },\n    \"page_on_front\": {\n        \"value\": \"5\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:07:08\"\n    }\n}','','','trash','closed','closed','','c338a5b4-fecd-44b7-a008-db7405590830','','','2018-09-12 16:07:08','2018-09-12 20:07:08','',0,'http://local.hasteandhustle.com/2018/09/12/c338a5b4-fecd-44b7-a008-db7405590830/',0,'customize_changeset','',0),
	(8,1,'2018-09-12 16:07:30','2018-09-12 20:07:30','{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:07:30\"\n    }\n}','','','trash','closed','closed','','58c7e3e9-db62-4f3a-bf94-7a2a7e4e2b57','','','2018-09-12 16:07:30','2018-09-12 20:07:30','',0,'http://local.hasteandhustle.com/2018/09/12/58c7e3e9-db62-4f3a-bf94-7a2a7e4e2b57/',0,'customize_changeset','',0),
	(10,1,'2018-09-12 16:11:35','2018-09-12 20:11:35','{\n    \"hastehustle::custom_logo\": {\n        \"value\": 9,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:11:35\"\n    }\n}','','','trash','closed','closed','','0bf3b07a-7888-49f0-b502-3749285078fa','','','2018-09-12 16:11:35','2018-09-12 20:11:35','',0,'http://local.hasteandhustle.com/2018/09/12/0bf3b07a-7888-49f0-b502-3749285078fa/',0,'customize_changeset','',0),
	(11,1,'2018-09-12 16:20:12','2018-09-12 20:20:12','{\n    \"hastehustle::custom_logo\": {\n        \"value\": \"\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:20:12\"\n    }\n}','','','trash','closed','closed','','ddcfd9eb-2076-4b3d-aa70-2b03db117cd4','','','2018-09-12 16:20:12','2018-09-12 20:20:12','',0,'http://local.hasteandhustle.com/2018/09/12/ddcfd9eb-2076-4b3d-aa70-2b03db117cd4/',0,'customize_changeset','',0),
	(12,1,'2018-09-12 16:24:15','2018-09-12 20:24:15','{\n    \"hastehustle::understrap_sidebar_position\": {\n        \"value\": \"none\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:24:15\"\n    }\n}','','','trash','closed','closed','','693af2ab-4f6b-4e6f-8922-3152070f336c','','','2018-09-12 16:24:15','2018-09-12 20:24:15','',0,'http://local.hasteandhustle.com/2018/09/12/693af2ab-4f6b-4e6f-8922-3152070f336c/',0,'customize_changeset','',0),
	(13,1,'2018-09-12 16:28:11','2018-09-12 20:28:11','Hey','Home','','inherit','closed','closed','','5-revision-v1','','','2018-09-12 16:28:11','2018-09-12 20:28:11','',5,'http://local.hasteandhustle.com/2018/09/12/5-revision-v1/',0,'revision','',0),
	(14,1,'2018-09-12 16:29:39','2018-09-12 20:29:39','{\n    \"hastehustle::custom_logo\": {\n        \"value\": 9,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:29:39\"\n    }\n}','','','trash','closed','closed','','beb20f65-2dfb-43b1-bf3e-6fd07c195f3e','','','2018-09-12 16:29:39','2018-09-12 20:29:39','',0,'http://local.hasteandhustle.com/2018/09/12/beb20f65-2dfb-43b1-bf3e-6fd07c195f3e/',0,'customize_changeset','',0),
	(15,1,'2018-09-12 16:29:56','2018-09-12 20:29:56','{\n    \"hastehustle::custom_logo\": {\n        \"value\": \"\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:29:56\"\n    }\n}','','','trash','closed','closed','','038d3024-5c05-4777-ba86-4ad29b338301','','','2018-09-12 16:29:56','2018-09-12 20:29:56','',0,'http://local.hasteandhustle.com/2018/09/12/038d3024-5c05-4777-ba86-4ad29b338301/',0,'customize_changeset','',0),
	(17,1,'2018-09-12 16:33:13','2018-09-12 20:33:13','{\n    \"hastehustle::custom_logo\": {\n        \"value\": 16,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-12 20:33:13\"\n    }\n}','','','trash','closed','closed','','86fbe354-d343-4494-955d-20e087c94617','','','2018-09-12 16:33:13','2018-09-12 20:33:13','',0,'http://local.hasteandhustle.com/2018/09/12/86fbe354-d343-4494-955d-20e087c94617/',0,'customize_changeset','',0),
	(19,1,'2018-09-12 16:42:56','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-09-12 16:42:56','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=19',1,'nav_menu_item','',0),
	(20,1,'2018-09-12 16:42:56','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-09-12 16:42:56','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=20',1,'nav_menu_item','',0),
	(21,1,'2018-09-12 16:45:31','2018-09-12 20:45:31','','Startup','','publish','closed','closed','','startup','','','2018-09-12 16:45:31','2018-09-12 20:45:31','',0,'http://local.hasteandhustle.com/?page_id=21',0,'page','',0),
	(22,1,'2018-09-12 16:45:31','2018-09-12 20:45:31','','Startup','','inherit','closed','closed','','21-revision-v1','','','2018-09-12 16:45:31','2018-09-12 20:45:31','',21,'http://local.hasteandhustle.com/2018/09/12/21-revision-v1/',0,'revision','',0),
	(23,1,'2018-09-12 16:45:47','2018-09-12 20:45:47','','Growth','','publish','closed','closed','','growth','','','2018-09-12 16:45:47','2018-09-12 20:45:47','',0,'http://local.hasteandhustle.com/?page_id=23',0,'page','',0),
	(24,1,'2018-09-12 16:45:47','2018-09-12 20:45:47','','Growth','','inherit','closed','closed','','23-revision-v1','','','2018-09-12 16:45:47','2018-09-12 20:45:47','',23,'http://local.hasteandhustle.com/2018/09/12/23-revision-v1/',0,'revision','',0),
	(25,1,'2018-09-12 16:46:06','2018-09-12 20:46:06','','Accelerate','','publish','closed','closed','','accelerate','','','2018-09-12 16:46:06','2018-09-12 20:46:06','',0,'http://local.hasteandhustle.com/?page_id=25',0,'page','',0),
	(26,1,'2018-09-12 16:46:06','2018-09-12 20:46:06','','Accelerate','','inherit','closed','closed','','25-revision-v1','','','2018-09-12 16:46:06','2018-09-12 20:46:06','',25,'http://local.hasteandhustle.com/2018/09/12/25-revision-v1/',0,'revision','',0),
	(27,1,'2018-09-12 16:51:48','2018-09-12 20:51:48','','Sell / IPO','','publish','closed','closed','','sell-ipo','','','2018-09-12 16:51:48','2018-09-12 20:51:48','',0,'http://local.hasteandhustle.com/?page_id=27',0,'page','',0),
	(28,1,'2018-09-12 16:51:48','2018-09-12 20:51:48','','Sell / IPO','','inherit','closed','closed','','27-revision-v1','','','2018-09-12 16:51:48','2018-09-12 20:51:48','',27,'http://local.hasteandhustle.com/2018/09/12/27-revision-v1/',0,'revision','',0),
	(29,1,'2018-09-12 16:52:00','2018-09-12 20:52:00','','Resources','','publish','closed','closed','','resources','','','2018-09-12 16:52:00','2018-09-12 20:52:00','',0,'http://local.hasteandhustle.com/?page_id=29',0,'page','',0),
	(30,1,'2018-09-12 16:52:00','2018-09-12 20:52:00','','Resources','','inherit','closed','closed','','29-revision-v1','','','2018-09-12 16:52:00','2018-09-12 20:52:00','',29,'http://local.hasteandhustle.com/2018/09/12/29-revision-v1/',0,'revision','',0),
	(37,1,'2018-09-17 14:39:49','2018-09-17 18:39:49','{\n    \"hastehustle::custom_logo\": {\n        \"value\": \"\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-17 18:39:49\"\n    }\n}','','','trash','closed','closed','','bf17dbbc-b51c-4510-ab20-fc40fd1e4fd2','','','2018-09-17 14:39:49','2018-09-17 18:39:49','',0,'http://local.hasteandhustle.com/2018/09/17/bf17dbbc-b51c-4510-ab20-fc40fd1e4fd2/',0,'customize_changeset','',0),
	(38,1,'2018-09-17 14:45:53','2018-09-17 18:45:53','','hh_logo','','inherit','open','closed','','hh_logo','','','2018-09-17 14:45:53','2018-09-17 18:45:53','',0,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/hh_logo.png',0,'attachment','image/png',0),
	(39,1,'2018-09-17 18:33:57','0000-00-00 00:00:00','','','','draft','closed','closed','','','','','2018-09-17 18:33:57','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=39',1,'nav_menu_item','',0),
	(45,1,'2018-09-18 13:21:18','2018-09-18 17:21:18','L orem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel tortor sit amet ante scelerisque placerat. Mauris ac euismod dui. Quisque placerat purus et nisl consectetur ullamcorper. Fusce porta nisl eget turpis hendrerit, nec gravida neque consectetur. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed diam lorem, iaculis id velit in, tincidunt iaculis ante. Quisque porttitor, eros eu tristique malesuada, purus risus tincidunt justo, vitae aliquet nulla arcu a arcu. Nam lacinia nisl a est maximus, lacinia convallis nisi euismod. Nulla at leo sit amet felis commodo mattis consequat eget urna. Cras libero sem, pulvinar ut turpis vitae, aliquam euismod arcu. Cras non posuere ipsum. Nulla eget diam lacus. Quisque aliquam ipsum pretium, rhoncus ante ac, accumsan quam.\r\n\r\nNam eleifend ligula in dui venenatis, in pellentesque justo sodales. Nam vestibulum ipsum risus. Nulla tempus tristique augue nec ultrices. Aliquam finibus quis sapien quis consectetur. Cras purus neque, tempus at congue at, congue varius nibh. Nulla consectetur, nulla ac faucibus tempor, sem massa lacinia mauris, at ornare velit leo eu urna. Fusce facilisis cursus dictum. Sed vel urna sed dui luctus convallis. Phasellus dignissim felis id lacinia ullamcorper. Aliquam ornare commodo purus congue ornare. Maecenas at facilisis dolor, sed placerat lacus. Aliquam at urna efficitur, facilisis libero nec, euismod velit. Curabitur vulputate enim eget posuere tincidunt. Vestibulum fringilla eu eros ut imperdiet. Cras mattis semper sapien, in aliquam elit posuere in. Suspendisse consectetur sapien et ante tristique, consequat tempus velit accumsan.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris et elit sed ipsum rhoncus tincidunt. Vestibulum vel massa in neque vulputate vulputate vitae tempor lectus. Proin id ornare lorem. Nullam sodales felis vel massa congue, eu accumsan lacus pulvinar. Suspendisse eu quam eu neque vulputate viverra. Nam tincidunt massa et pellentesque rhoncus. Integer non pretium ligula, vel pretium lorem. Vestibulum ante lacus, dignissim eu dolor eget, consequat sodales justo.\r\n\r\nQuisque sit amet dolor a nunc viverra commodo. Ut id viverra lectus. In venenatis ex sed nisl varius, non sollicitudin purus condimentum. Sed dictum ut nisl eget sollicitudin. Mauris pellentesque diam odio, at tincidunt lacus elementum vitae. Vivamus volutpat fermentum auctor. Nullam varius lacus non odio sodales, in semper turpis hendrerit. Mauris vitae felis nec felis finibus tincidunt ac sed nisl. Quisque eu est tortor. Nullam pretium nec arcu non ornare. Ut fermentum enim est, id feugiat neque pellentesque id. In ornare, quam eget placerat interdum, mi risus dictum lectus, sed scelerisque magna ante quis ligula. Vestibulum at laoreet massa, vitae pharetra enim. Duis pretium, velit quis rutrum tempor, felis sem lacinia ex, at interdum eros lectus et dui. Integer quis fermentum nibh, et feugiat nunc.\r\n\r\nMaecenas mollis lectus quis sem dictum, non posuere neque vulputate. Suspendisse id facilisis purus. Nam tincidunt mi elementum lectus aliquam, id tincidunt neque egestas. Nullam blandit lacinia rhoncus. Nunc porta ornare leo hendrerit efficitur. Sed vitae metus dui. Quisque tempor bibendum justo, ut malesuada odio. Proin vitae mauris faucibus, commodo diam id, pharetra elit. In semper bibendum nulla et interdum. Phasellus ipsum elit, elementum nec eros convallis, efficitur porttitor velit. Donec pharetra et nisi quis auctor. Sed pharetra mattis neque id tristique. Phasellus eget erat imperdiet, maximus orci in, mollis massa. Aliquam sodales vitae lacus sed sodales. Mauris malesuada fringilla diam eu maximus.','Fusce porta nisl eget turpis hendrerit, nec gravida neque consectetur. Orci varius natoque penatibus et magnis dis parturient montes.','','publish','open','open','','fusce-porta-nisl-eget-turpis-hendrerit-nec-gravida-neque-consectetur-orci-varius-natoque-penatibus-et-magnis-dis-parturient-montes','','','2018-09-18 13:40:14','2018-09-18 17:40:14','',0,'http://local.hasteandhustle.com/?p=45',0,'post','',0),
	(46,1,'2018-09-18 13:20:12','2018-09-18 17:20:12','','sample-hero@2x','','inherit','open','closed','','sample-hero2x','','','2018-09-18 13:20:12','2018-09-18 17:20:12','',45,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/sample-hero@2x.jpg',0,'attachment','image/jpeg',0),
	(47,1,'2018-09-18 13:20:49','2018-09-18 17:20:49','L orem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel tortor sit amet ante scelerisque placerat. Mauris ac euismod dui. Quisque placerat purus et nisl consectetur ullamcorper. Fusce porta nisl eget turpis hendrerit, nec gravida neque consectetur. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed diam lorem, iaculis id velit in, tincidunt iaculis ante. Quisque porttitor, eros eu tristique malesuada, purus risus tincidunt justo, vitae aliquet nulla arcu a arcu. Nam lacinia nisl a est maximus, lacinia convallis nisi euismod. Nulla at leo sit amet felis commodo mattis consequat eget urna. Cras libero sem, pulvinar ut turpis vitae, aliquam euismod arcu. Cras non posuere ipsum. Nulla eget diam lacus. Quisque aliquam ipsum pretium, rhoncus ante ac, accumsan quam.\r\n\r\nNam eleifend ligula in dui venenatis, in pellentesque justo sodales. Nam vestibulum ipsum risus. Nulla tempus tristique augue nec ultrices. Aliquam finibus quis sapien quis consectetur. Cras purus neque, tempus at congue at, congue varius nibh. Nulla consectetur, nulla ac faucibus tempor, sem massa lacinia mauris, at ornare velit leo eu urna. Fusce facilisis cursus dictum. Sed vel urna sed dui luctus convallis. Phasellus dignissim felis id lacinia ullamcorper. Aliquam ornare commodo purus congue ornare. Maecenas at facilisis dolor, sed placerat lacus. Aliquam at urna efficitur, facilisis libero nec, euismod velit. Curabitur vulputate enim eget posuere tincidunt. Vestibulum fringilla eu eros ut imperdiet. Cras mattis semper sapien, in aliquam elit posuere in. Suspendisse consectetur sapien et ante tristique, consequat tempus velit accumsan.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris et elit sed ipsum rhoncus tincidunt. Vestibulum vel massa in neque vulputate vulputate vitae tempor lectus. Proin id ornare lorem. Nullam sodales felis vel massa congue, eu accumsan lacus pulvinar. Suspendisse eu quam eu neque vulputate viverra. Nam tincidunt massa et pellentesque rhoncus. Integer non pretium ligula, vel pretium lorem. Vestibulum ante lacus, dignissim eu dolor eget, consequat sodales justo.\r\n\r\nQuisque sit amet dolor a nunc viverra commodo. Ut id viverra lectus. In venenatis ex sed nisl varius, non sollicitudin purus condimentum. Sed dictum ut nisl eget sollicitudin. Mauris pellentesque diam odio, at tincidunt lacus elementum vitae. Vivamus volutpat fermentum auctor. Nullam varius lacus non odio sodales, in semper turpis hendrerit. Mauris vitae felis nec felis finibus tincidunt ac sed nisl. Quisque eu est tortor. Nullam pretium nec arcu non ornare. Ut fermentum enim est, id feugiat neque pellentesque id. In ornare, quam eget placerat interdum, mi risus dictum lectus, sed scelerisque magna ante quis ligula. Vestibulum at laoreet massa, vitae pharetra enim. Duis pretium, velit quis rutrum tempor, felis sem lacinia ex, at interdum eros lectus et dui. Integer quis fermentum nibh, et feugiat nunc.\r\n\r\nMaecenas mollis lectus quis sem dictum, non posuere neque vulputate. Suspendisse id facilisis purus. Nam tincidunt mi elementum lectus aliquam, id tincidunt neque egestas. Nullam blandit lacinia rhoncus. Nunc porta ornare leo hendrerit efficitur. Sed vitae metus dui. Quisque tempor bibendum justo, ut malesuada odio. Proin vitae mauris faucibus, commodo diam id, pharetra elit. In semper bibendum nulla et interdum. Phasellus ipsum elit, elementum nec eros convallis, efficitur porttitor velit. Donec pharetra et nisi quis auctor. Sed pharetra mattis neque id tristique. Phasellus eget erat imperdiet, maximus orci in, mollis massa. Aliquam sodales vitae lacus sed sodales. Mauris malesuada fringilla diam eu maximus.','Fusce porta nisl eget turpis hendrerit, nec gravida neque consectetur. Orci varius natoque penatibus et magnis dis parturient montes.','','inherit','closed','closed','','45-revision-v1','','','2018-09-18 13:20:49','2018-09-18 17:20:49','',45,'http://local.hasteandhustle.com/2018/09/18/45-revision-v1/',0,'revision','',0),
	(48,1,'2018-09-18 13:25:20','2018-09-18 17:25:20','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Magna ac placerat vestibulum lectus mauris. Rhoncus urna neque viverra justo nec ultrices. Nec ullamcorper sit amet risus. Et malesuada fames ac turpis egestas. Imperdiet nulla malesuada pellentesque elit eget gravida cum. Tortor aliquam nulla facilisi cras. Placerat in egestas erat imperdiet sed euismod nisi. Est placerat in egestas erat imperdiet sed euismod nisi porta. Volutpat lacus laoreet non curabitur. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Porttitor leo a diam sollicitudin tempor. Libero id faucibus nisl tincidunt eget nullam. Accumsan lacus vel facilisis volutpat est velit egestas dui id.\r\n\r\nQuis blandit turpis cursus in. Magna etiam tempor orci eu. Ornare massa eget egestas purus viverra accumsan. Tempus iaculis urna id volutpat lacus. Ut tristique et egestas quis ipsum suspendisse. Pellentesque id nibh tortor id aliquet lectus proin nibh nisl. Vel risus commodo viverra maecenas accumsan. Iaculis urna id volutpat lacus laoreet. Ornare lectus sit amet est placerat in egestas erat imperdiet. Eros donec ac odio tempor orci dapibus ultrices. Neque vitae tempus quam pellentesque nec nam aliquam.\r\n\r\nAt augue eget arcu dictum. Lectus nulla at volutpat diam ut venenatis. Nunc mattis enim ut tellus elementum sagittis vitae. Quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse. Sed risus pretium quam vulputate dignissim suspendisse. In hendrerit gravida rutrum quisque non tellus orci ac. Tortor condimentum lacinia quis vel eros donec ac odio. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Iaculis at erat pellentesque adipiscing commodo elit at. Amet nisl suscipit adipiscing bibendum. Purus non enim praesent elementum. Massa vitae tortor condimentum lacinia quis vel eros. Orci porta non pulvinar neque. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Leo vel orci porta non pulvinar neque laoreet suspendisse interdum. Risus pretium quam vulputate dignissim. Sapien pellentesque habitant morbi tristique senectus et netus et. Interdum velit euismod in pellentesque massa.\r\n\r\nTincidunt dui ut ornare lectus sit amet est placerat in. A diam maecenas sed enim ut sem viverra aliquet. Amet cursus sit amet dictum sit amet justo donec enim. Tincidunt ornare massa eget egestas purus viverra. Facilisis leo vel fringilla est ullamcorper eget. Non consectetur a erat nam at lectus. Pharetra convallis posuere morbi leo urna molestie at. Nibh tortor id aliquet lectus. Odio tempor orci dapibus ultrices in iaculis nunc. Libero id faucibus nisl tincidunt eget. Tincidunt eget nullam non nisi est sit amet facilisis magna. Eros donec ac odio tempor. A diam maecenas sed enim ut sem viverra aliquet. Quis vel eros donec ac odio tempor orci dapibus. Sed blandit libero volutpat sed cras ornare. Eu scelerisque felis imperdiet proin fermentum leo vel orci. Sagittis vitae et leo duis. Enim ut sem viverra aliquet eget sit amet tellus cras. Quam pellentesque nec nam aliquam sem et tortor consequat id.\r\n\r\nOrnare arcu odio ut sem nulla pharetra. Quis auctor elit sed vulputate mi sit amet mauris. Ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Pellentesque diam volutpat commodo sed egestas egestas fringilla. Tristique sollicitudin nibh sit amet commodo nulla facilisi. Ut consequat semper viverra nam libero justo laoreet sit. Eget velit aliquet sagittis id consectetur. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Euismod in pellentesque massa placerat duis ultricies lacus. Feugiat vivamus at augue eget arcu dictum varius duis at. Gravida arcu ac tortor dignissim convallis aenean et. Scelerisque felis imperdiet proin fermentum leo vel orci. Urna et pharetra pharetra massa massa ultricies mi. Id aliquet risus feugiat in ante metus dictum. Iaculis nunc sed augue lacus viverra. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt.','Magna ac placerat vestibulum lectus mauris','','publish','open','open','','magna-ac-placerat-vestibulum-lectus-mauris','','','2018-09-21 17:27:29','2018-09-21 21:27:29','',0,'http://local.hasteandhustle.com/?p=48',0,'post','',0),
	(49,1,'2018-09-18 13:25:13','2018-09-18 17:25:13','','secondary-hero@2x','','inherit','open','closed','','secondary-hero2x','','','2018-09-18 13:25:13','2018-09-18 17:25:13','',48,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/secondary-hero@2x.jpg',0,'attachment','image/jpeg',0),
	(50,1,'2018-09-18 13:25:20','2018-09-18 17:25:20','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Magna ac placerat vestibulum lectus mauris. Rhoncus urna neque viverra justo nec ultrices. Nec ullamcorper sit amet risus. Et malesuada fames ac turpis egestas. Imperdiet nulla malesuada pellentesque elit eget gravida cum. Tortor aliquam nulla facilisi cras. Placerat in egestas erat imperdiet sed euismod nisi. Est placerat in egestas erat imperdiet sed euismod nisi porta. Volutpat lacus laoreet non curabitur. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Porttitor leo a diam sollicitudin tempor. Libero id faucibus nisl tincidunt eget nullam. Accumsan lacus vel facilisis volutpat est velit egestas dui id.\r\n\r\nQuis blandit turpis cursus in. Magna etiam tempor orci eu. Ornare massa eget egestas purus viverra accumsan. Tempus iaculis urna id volutpat lacus. Ut tristique et egestas quis ipsum suspendisse. Pellentesque id nibh tortor id aliquet lectus proin nibh nisl. Vel risus commodo viverra maecenas accumsan. Iaculis urna id volutpat lacus laoreet. Ornare lectus sit amet est placerat in egestas erat imperdiet. Eros donec ac odio tempor orci dapibus ultrices. Neque vitae tempus quam pellentesque nec nam aliquam.\r\n\r\nAt augue eget arcu dictum. Lectus nulla at volutpat diam ut venenatis. Nunc mattis enim ut tellus elementum sagittis vitae. Quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse. Sed risus pretium quam vulputate dignissim suspendisse. In hendrerit gravida rutrum quisque non tellus orci ac. Tortor condimentum lacinia quis vel eros donec ac odio. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Iaculis at erat pellentesque adipiscing commodo elit at. Amet nisl suscipit adipiscing bibendum. Purus non enim praesent elementum. Massa vitae tortor condimentum lacinia quis vel eros. Orci porta non pulvinar neque. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Leo vel orci porta non pulvinar neque laoreet suspendisse interdum. Risus pretium quam vulputate dignissim. Sapien pellentesque habitant morbi tristique senectus et netus et. Interdum velit euismod in pellentesque massa.\r\n\r\nTincidunt dui ut ornare lectus sit amet est placerat in. A diam maecenas sed enim ut sem viverra aliquet. Amet cursus sit amet dictum sit amet justo donec enim. Tincidunt ornare massa eget egestas purus viverra. Facilisis leo vel fringilla est ullamcorper eget. Non consectetur a erat nam at lectus. Pharetra convallis posuere morbi leo urna molestie at. Nibh tortor id aliquet lectus. Odio tempor orci dapibus ultrices in iaculis nunc. Libero id faucibus nisl tincidunt eget. Tincidunt eget nullam non nisi est sit amet facilisis magna. Eros donec ac odio tempor. A diam maecenas sed enim ut sem viverra aliquet. Quis vel eros donec ac odio tempor orci dapibus. Sed blandit libero volutpat sed cras ornare. Eu scelerisque felis imperdiet proin fermentum leo vel orci. Sagittis vitae et leo duis. Enim ut sem viverra aliquet eget sit amet tellus cras. Quam pellentesque nec nam aliquam sem et tortor consequat id.\r\n\r\nOrnare arcu odio ut sem nulla pharetra. Quis auctor elit sed vulputate mi sit amet mauris. Ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Pellentesque diam volutpat commodo sed egestas egestas fringilla. Tristique sollicitudin nibh sit amet commodo nulla facilisi. Ut consequat semper viverra nam libero justo laoreet sit. Eget velit aliquet sagittis id consectetur. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Euismod in pellentesque massa placerat duis ultricies lacus. Feugiat vivamus at augue eget arcu dictum varius duis at. Gravida arcu ac tortor dignissim convallis aenean et. Scelerisque felis imperdiet proin fermentum leo vel orci. Urna et pharetra pharetra massa massa ultricies mi. Id aliquet risus feugiat in ante metus dictum. Iaculis nunc sed augue lacus viverra. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt.','Magna ac placerat vestibulum lectus mauris','','inherit','closed','closed','','48-revision-v1','','','2018-09-18 13:25:20','2018-09-18 17:25:20','',48,'http://local.hasteandhustle.com/48-revision-v1/',0,'revision','',0),
	(51,1,'2018-09-19 11:55:09','2018-09-19 15:55:09','','ex-prof-pic','','inherit','open','closed','','ex-prof-pic','','','2018-09-19 11:55:09','2018-09-19 15:55:09','',0,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/ex-prof-pic.jpg',0,'attachment','image/jpeg',0),
	(52,1,'2018-09-21 18:14:56','2018-09-21 22:14:56','','lastest-new-bkg','','inherit','open','closed','','lastest-new-bkg','','','2018-09-21 18:14:56','2018-09-21 22:14:56','',0,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/lastest-new-bkg.jpg',0,'attachment','image/jpeg',0),
	(53,1,'2018-09-21 18:34:22','2018-09-21 22:34:22','Like his nickname suggests, Manu ‘Swish’ Goswami is a young entrepreneur unafraid to take big shots and score his share of successes in the business world. Tapped up on CBC’s Dragons Den as a future prime minister due to his wealth of ideas, this 20-year-old is already doing it all. A TEDx speaker, venture capitalist, LinkedIn Youth Editor (over 50,000 followers) and a United Nations youth ambassador, Manu Goswami defies logic with his abilities to create things on a dime. You will be blown away hearing one of the most accomplished young entrepreneurs around speak as a Generation Now 2018 headliner.\r\n<h3><b>Venturing From an Early Age</b></h3>\r\nA native of Singapore, Goswami cheekily started his first business at age seven by taking his neighbour’s flowers from their lawn, slapping a bow on them and selling them back. As you would imagine, this venture was quelled soon after that, but his thirst for hustle and invention was evident. At 10 years old, Goswami founded a hovercraft business, for which some proceeds went to a local orphanage in his homeland. At 14, he was part of a company that used its fervour for making a social impact, becoming a nominee for Canada’s Company of the Year. Goswami has never believed that age should hinder your ability to create groundbreaking, money-making ventures. Armed with that belief, he has become an entrepreneur ahead of his time.\r\n<h3><b>Multi-Tasking His Way To Success</b></h3>\r\nGoswami has co-founded six entities. These include the World Youth Fund, the first worldwide youth social capital fund, RafikiMedia, a social-first digital shop, and FoodShare, an app allowing you to access affordable and accessible food options. His other notable ventures include The Next Foundry, a financing factory that financed 76 early-stage technology and social ventures, as well as World Thinks and GenSys. As a result of his focus and passion for creating early-stage ventures, Swish was recognized in 2015 as one of Canada’s top 20 under 20 and has become one of Canada’s fast emerging entrepreneurs.\r\n\r\nGoswami’s businesses have connected entrepreneurs all over the worlds and financed initiatives worldwide. He also is a venture capitalist for JB Fitzgerald Venture Capital, founded by Philadelphia 76ers power forward, Trevor Booker. Plus, he founded a sports media conglomerate called Dunk Media, which is heavily focused on social interaction and features over 10 million basketball-crazy fans. It’s been said you miss 100% of the shots you don’t take, and Swish isn’t afraid to take his share of them, coming up in the clutch, and often changing business world.\r\n<h3><b>Advising, Advocacy, and Acclaim</b></h3>\r\nGoswami also serves as an advisor and board member of several organizations, including Children of Hope Uganda, Beat The Street, the Literacy Empowers Foundation, and the Canadian Student Debating Foundation. He also is a contributor to the Huffington Post, Globe and Mail and hosts his own series of podcasts and videos.\r\n\r\nFurthermore, Goswami is an advocate for youth entrepreneurship and mental health. He has been able to reach thousands of children worldwide by being a National Youth Ambassador for the Speak Your Mind project, Alberta’s Ministry of Education and many more.\r\n\r\nHe has been recommended by Forbes Magazine as ‘one of the ten Gen Z experts to follow’ and the ‘Face and Future of Canadian Entrepreneurship’ by UPS Canada. Simply put, Goswami is a born leader who believes that while young people are the leaders of tomorrow, they should not wait until tomorrow to lead. You shouldn’t wait either!\r\n\r\nGet tickets to see Swish on stage at Generation Now, <a href=\"http://hasteandhustle.com/2018/02/13/845-our-top-3-reasons-to-attend-generation-now/\">one of the most insightful conferences for entrepreneurs</a>, and become inspired!','Meet Our Speakers: Young Entrepreneur Manu (Swish) Goswami','','publish','open','open','','meet-our-speakers-young-entrepreneur-manu-swish-goswami','','','2018-09-21 18:34:22','2018-09-21 22:34:22','',0,'http://local.hasteandhustle.com/?p=53',0,'post','',0),
	(54,1,'2018-09-21 18:34:12','2018-09-21 22:34:12','','Manu-Goswami','','inherit','open','closed','','manu-goswami','','','2018-09-21 18:34:12','2018-09-21 22:34:12','',53,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/Manu-Goswami.jpg',0,'attachment','image/jpeg',0),
	(55,1,'2018-09-21 18:34:22','2018-09-21 22:34:22','Like his nickname suggests, Manu ‘Swish’ Goswami is a young entrepreneur unafraid to take big shots and score his share of successes in the business world. Tapped up on CBC’s Dragons Den as a future prime minister due to his wealth of ideas, this 20-year-old is already doing it all. A TEDx speaker, venture capitalist, LinkedIn Youth Editor (over 50,000 followers) and a United Nations youth ambassador, Manu Goswami defies logic with his abilities to create things on a dime. You will be blown away hearing one of the most accomplished young entrepreneurs around speak as a Generation Now 2018 headliner.\r\n<h3><b>Venturing From an Early Age</b></h3>\r\nA native of Singapore, Goswami cheekily started his first business at age seven by taking his neighbour’s flowers from their lawn, slapping a bow on them and selling them back. As you would imagine, this venture was quelled soon after that, but his thirst for hustle and invention was evident. At 10 years old, Goswami founded a hovercraft business, for which some proceeds went to a local orphanage in his homeland. At 14, he was part of a company that used its fervour for making a social impact, becoming a nominee for Canada’s Company of the Year. Goswami has never believed that age should hinder your ability to create groundbreaking, money-making ventures. Armed with that belief, he has become an entrepreneur ahead of his time.\r\n<h3><b>Multi-Tasking His Way To Success</b></h3>\r\nGoswami has co-founded six entities. These include the World Youth Fund, the first worldwide youth social capital fund, RafikiMedia, a social-first digital shop, and FoodShare, an app allowing you to access affordable and accessible food options. His other notable ventures include The Next Foundry, a financing factory that financed 76 early-stage technology and social ventures, as well as World Thinks and GenSys. As a result of his focus and passion for creating early-stage ventures, Swish was recognized in 2015 as one of Canada’s top 20 under 20 and has become one of Canada’s fast emerging entrepreneurs.\r\n\r\nGoswami’s businesses have connected entrepreneurs all over the worlds and financed initiatives worldwide. He also is a venture capitalist for JB Fitzgerald Venture Capital, founded by Philadelphia 76ers power forward, Trevor Booker. Plus, he founded a sports media conglomerate called Dunk Media, which is heavily focused on social interaction and features over 10 million basketball-crazy fans. It’s been said you miss 100% of the shots you don’t take, and Swish isn’t afraid to take his share of them, coming up in the clutch, and often changing business world.\r\n<h3><b>Advising, Advocacy, and Acclaim</b></h3>\r\nGoswami also serves as an advisor and board member of several organizations, including Children of Hope Uganda, Beat The Street, the Literacy Empowers Foundation, and the Canadian Student Debating Foundation. He also is a contributor to the Huffington Post, Globe and Mail and hosts his own series of podcasts and videos.\r\n\r\nFurthermore, Goswami is an advocate for youth entrepreneurship and mental health. He has been able to reach thousands of children worldwide by being a National Youth Ambassador for the Speak Your Mind project, Alberta’s Ministry of Education and many more.\r\n\r\nHe has been recommended by Forbes Magazine as ‘one of the ten Gen Z experts to follow’ and the ‘Face and Future of Canadian Entrepreneurship’ by UPS Canada. Simply put, Goswami is a born leader who believes that while young people are the leaders of tomorrow, they should not wait until tomorrow to lead. You shouldn’t wait either!\r\n\r\nGet tickets to see Swish on stage at Generation Now, <a href=\"http://hasteandhustle.com/2018/02/13/845-our-top-3-reasons-to-attend-generation-now/\">one of the most insightful conferences for entrepreneurs</a>, and become inspired!','Meet Our Speakers: Young Entrepreneur Manu (Swish) Goswami','','inherit','closed','closed','','53-revision-v1','','','2018-09-21 18:34:22','2018-09-21 22:34:22','',53,'http://local.hasteandhustle.com/53-revision-v1/',0,'revision','',0),
	(56,1,'2018-09-21 18:41:29','2018-09-21 22:41:29','','Home','','inherit','closed','closed','','5-revision-v1','','','2018-09-21 18:41:29','2018-09-21 22:41:29','',5,'http://local.hasteandhustle.com/5-revision-v1/',0,'revision','',0),
	(57,1,'2018-09-21 18:46:49','2018-09-21 22:46:49','Welcome to WordPress. This is your first post. Edit or delete it, then start writing!','Hello world!','','inherit','closed','closed','','1-revision-v1','','','2018-09-21 18:46:49','2018-09-21 22:46:49','',1,'http://local.hasteandhustle.com/1-revision-v1/',0,'revision','',0),
	(58,1,'2018-09-21 18:52:57','2018-09-21 22:52:57','','Our Top 3 Reasons To Attend Generation Now','','publish','open','open','','our-top-3-reasons-to-attend-generation-now','','','2018-10-11 16:40:51','2018-10-11 20:40:51','',0,'http://local.hasteandhustle.com/?p=58',0,'post','',0),
	(59,1,'2018-09-21 18:52:50','2018-09-21 22:52:50','','generation_now_interac','','inherit','open','closed','','generation_now_interac','','','2018-09-21 18:52:50','2018-09-21 22:52:50','',58,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/generation_now_interac.png',0,'attachment','image/png',0),
	(60,1,'2018-09-21 18:52:57','2018-09-21 22:52:57','Amidst a sea of competition, it helps to have guides to give you that extra boost to keep you afloat as an ever expansive business world takes shape. With an array of emerging entrepreneurs and promoting self-focused entrepreneurship development, Generation Now is one the premier conferences for entrepreneurs to attend.\r\n\r\nYou will meet entrepreneurs from all walks of life, some who have made it against tough odds to make their businesses visible and profitable. Here are three reasons you should attend Generation Now to get your business booming in 2018.\r\n<h3><b>Networking Opportunities</b></h3>\r\nThrough Generation Now, you will have chances to interact with industry insiders through a series of talks and learning sessions. In a fun, engaging, energetic atmosphere, you will be able to network with trailblazers in the industry who will help fast-track your success. You will learn tips on how to build and promote your start-up business, offer strategic business development pointers to ensure your venture grows organically, and much more.\r\n\r\nFor two days, you will be introduced to ways of thinking you perhaps have never been exposed to and be able to present ideas to people who have been in your shoes. People who know what it’s like to go against the grain, fight obstacles with ideas and become successful. These networking opportunities are unrivaled and should be cherished far beyond these 48 hours.\r\n<h3><b>Industry Leading Speakers</b></h3>\r\nThe best inspiration you can get is often provided from people who know your struggle. Generation Now will allow you to be able to relate to several entrepreneurs who faced those hardships and overcame them. People who not only used good marketing and strategy to turn their businesses into goldmines but used initiative and thought outside the box to do it.\r\n\r\nPeople like <a href=\"http://hasteandhustle.com/2018/01/24/meet-speakers-amy-birks-business-strategy-ninja/\">Amy Birks, the Strategy Ninja</a>, a best-selling author and business consultant who is proof that you can do the hustle without the hassle. People like <a href=\"http://hasteandhustle.com/2018/01/05/7880-meet-speakers-young-entrepreneur-connor-blakley/\">Connor Blakely</a>, who at 18 years old is already a CEO of a youth marketing consultancy. People like <a href=\"http://hasteandhustle.com/2017/09/20/speakers-gary-vaynerchuk/\">Gary Vaynerchuk</a>, who owns his own digital media agency and helped grow his family’s wine business from being a three million dollar entity into a 60 million dollar entity within five years. People like Manu Goswami, a University of Toronto student who is one of the world’s youngest venture capitalists at age 19 and owns several successful ventures. These are people who not only know what they’re talking about but people who will share their stories and make you understand the dedication required to make it in the entrepreneurial world, regardless of age.\r\n<h3><b>Knowledge You Can’t Get Anywhere Else</b></h3>\r\nAll these speakers and more will provide you with strategies and advice you probably have never been privy to before. These speakers were chosen with care and with a view to enlighten you to new ways of thinking, ways a book or blueprint won’t necessarily teach you. They all had ideas and plans but also didn’t let anyone else define their measures for success. To be able to mingle and pick the brains of such amazing people will open your minds to new possibilities and give you that extra desire to see your business grow, your way.\r\n\r\nGet your tickets to Generation Now today and see why it’s so highly recommended. When you leave the Sony Centre after this conference, you will gain a whole new perspective on entrepreneurship that will live long in the memory!','Our Top 3 Reasons To Attend Generation Now','','inherit','closed','closed','','58-revision-v1','','','2018-09-21 18:52:57','2018-09-21 22:52:57','',58,'http://local.hasteandhustle.com/58-revision-v1/',0,'revision','',0),
	(61,1,'2018-09-24 17:24:38','2018-09-24 21:24:38','','Contribute','','publish','closed','closed','','contribute','','','2018-09-24 17:24:38','2018-09-24 21:24:38','',0,'http://local.hasteandhustle.com/?page_id=61',0,'page','',0),
	(62,1,'2018-09-24 17:24:38','2018-09-24 21:24:38','','Contribute','','inherit','closed','closed','','61-revision-v1','','','2018-09-24 17:24:38','2018-09-24 21:24:38','',61,'http://local.hasteandhustle.com/61-revision-v1/',0,'revision','',0),
	(63,1,'2018-09-24 17:24:47','2018-09-24 21:24:47','','Contact','','trash','closed','closed','','contact__trashed','','','2018-10-09 09:54:32','2018-10-09 13:54:32','',0,'http://local.hasteandhustle.com/?page_id=63',0,'page','',0),
	(64,1,'2018-09-24 17:24:47','2018-09-24 21:24:47','','Contact','','inherit','closed','closed','','63-revision-v1','','','2018-09-24 17:24:47','2018-09-24 21:24:47','',63,'http://local.hasteandhustle.com/63-revision-v1/',0,'revision','',0),
	(65,1,'2018-09-24 17:24:56','2018-09-24 21:24:56','','Advertise','','publish','closed','closed','','advertise','','','2018-09-24 17:24:56','2018-09-24 21:24:56','',0,'http://local.hasteandhustle.com/?page_id=65',0,'page','',0),
	(66,1,'2018-09-24 17:24:56','2018-09-24 21:24:56','','Advertise','','inherit','closed','closed','','65-revision-v1','','','2018-09-24 17:24:56','2018-09-24 21:24:56','',65,'http://local.hasteandhustle.com/65-revision-v1/',0,'revision','',0),
	(67,1,'2018-09-24 17:25:40','2018-09-24 21:25:40',' ','','','publish','closed','closed','','67','','','2018-10-11 17:21:37','2018-10-11 21:21:37','',0,'http://local.hasteandhustle.com/?p=67',3,'nav_menu_item','',0),
	(69,1,'2018-09-24 17:25:40','2018-09-24 21:25:40',' ','','','publish','closed','closed','','69','','','2018-10-11 17:21:37','2018-10-11 21:21:37','',0,'http://local.hasteandhustle.com/?p=69',1,'nav_menu_item','',0),
	(75,1,'2018-09-24 18:37:34','2018-09-24 22:37:34','','<i class=\"fa fa-search\"></i>','','publish','closed','closed','','75','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=75',8,'nav_menu_item','',0),
	(78,1,'2018-09-24 18:45:37','2018-09-24 22:45:37','','Resources','','publish','closed','closed','','resources','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=78',3,'nav_menu_item','',0),
	(80,1,'2018-10-09 09:44:44','2018-10-09 13:44:44','','Contact Us','','publish','closed','closed','','contact-us','','','2018-10-09 09:44:44','2018-10-09 13:44:44','',0,'http://local.hasteandhustle.com/?page_id=80',0,'page','',0),
	(81,1,'2018-10-09 09:44:44','2018-10-09 13:44:44','','Contact Us','','inherit','closed','closed','','80-revision-v1','','','2018-10-09 09:44:44','2018-10-09 13:44:44','',80,'http://local.hasteandhustle.com/80-revision-v1/',0,'revision','',0),
	(82,1,'2018-10-09 09:44:53','2018-10-09 13:44:53','','Gigs','','publish','closed','closed','','gigs','','','2018-10-09 09:44:53','2018-10-09 13:44:53','',0,'http://local.hasteandhustle.com/?page_id=82',0,'page','',0),
	(83,1,'2018-10-09 09:44:53','2018-10-09 13:44:53','','Gigs','','inherit','closed','closed','','82-revision-v1','','','2018-10-09 09:44:53','2018-10-09 13:44:53','',82,'http://local.hasteandhustle.com/82-revision-v1/',0,'revision','',0),
	(84,1,'2018-10-09 09:47:24','2018-10-09 13:47:24','','Blog','','publish','closed','closed','','blog','','','2018-10-09 09:47:24','2018-10-09 13:47:24','',0,'http://local.hasteandhustle.com/?page_id=84',0,'page','',0),
	(85,1,'2018-10-09 09:47:24','2018-10-09 13:47:24','','Blog','','inherit','closed','closed','','84-revision-v1','','','2018-10-09 09:47:24','2018-10-09 13:47:24','',84,'http://local.hasteandhustle.com/84-revision-v1/',0,'revision','',0),
	(86,1,'2018-10-09 09:48:01','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-10-09 09:48:01','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=86',1,'nav_menu_item','',0),
	(87,1,'2018-10-09 09:53:28','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-10-09 09:53:28','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=87',1,'nav_menu_item','',0),
	(88,1,'2018-10-09 09:53:28','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-10-09 09:53:28','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=88',1,'nav_menu_item','',0),
	(89,1,'2018-10-09 09:53:28','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2018-10-09 09:53:28','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?p=89',1,'nav_menu_item','',0),
	(90,1,'2018-10-09 09:54:03','2018-10-09 13:54:03','','Business Stages','','publish','closed','closed','','business-stages','','','2018-10-09 09:54:03','2018-10-09 13:54:03','',0,'http://local.hasteandhustle.com/?page_id=90',0,'page','',0),
	(91,1,'2018-10-09 09:54:03','2018-10-09 13:54:03','','Business Stages','','inherit','closed','closed','','90-revision-v1','','','2018-10-09 09:54:03','2018-10-09 13:54:03','',90,'http://local.hasteandhustle.com/90-revision-v1/',0,'revision','',0),
	(92,1,'2018-10-09 09:54:25','2018-10-09 13:54:25',' ','','','publish','closed','closed','','92','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=92',2,'nav_menu_item','',0),
	(93,1,'2018-10-09 09:54:25','2018-10-09 13:54:25',' ','','','publish','closed','closed','','93','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=93',4,'nav_menu_item','',0),
	(94,1,'2018-10-09 09:54:25','2018-10-09 13:54:25',' ','','','publish','closed','closed','','94','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=94',6,'nav_menu_item','',0),
	(95,1,'2018-10-09 09:54:25','2018-10-09 13:54:25',' ','','','publish','closed','closed','','95','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=95',7,'nav_menu_item','',0),
	(96,1,'2018-10-09 09:55:33','2018-10-09 13:55:33',' ','','','publish','closed','closed','','96','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=96',1,'nav_menu_item','',0),
	(97,1,'2018-10-09 09:56:29','2018-10-09 13:56:29','','Interviews','','publish','closed','closed','','interviews','','','2018-10-09 09:56:29','2018-10-09 13:56:29','',0,'http://local.hasteandhustle.com/?page_id=97',0,'page','',0),
	(98,1,'2018-10-09 09:56:29','2018-10-09 13:56:29','','Interviews','','inherit','closed','closed','','97-revision-v1','','','2018-10-09 09:56:29','2018-10-09 13:56:29','',97,'http://local.hasteandhustle.com/97-revision-v1/',0,'revision','',0),
	(99,1,'2018-10-09 09:57:02','2018-10-09 13:57:02',' ','','','publish','closed','closed','','99','','','2018-10-09 09:57:02','2018-10-09 13:57:02','',0,'http://local.hasteandhustle.com/?p=99',5,'nav_menu_item','',0),
	(100,1,'2018-10-09 09:57:11','2018-10-09 13:57:11','','Events','','publish','closed','closed','','events','','','2018-10-09 09:57:11','2018-10-09 13:57:11','',0,'http://local.hasteandhustle.com/?page_id=100',0,'page','',0),
	(101,1,'2018-10-09 09:57:11','2018-10-09 13:57:11','','Events','','inherit','closed','closed','','100-revision-v1','','','2018-10-09 09:57:11','2018-10-09 13:57:11','',100,'http://local.hasteandhustle.com/100-revision-v1/',0,'revision','',0),
	(102,1,'2018-10-09 10:05:57','2018-10-09 14:05:57','{\n    \"hastehustle::understrap_container_type\": {\n        \"value\": \"container-fluid\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-10-09 14:05:57\"\n    }\n}','','','trash','closed','closed','','8bf22a34-1e9e-4b51-be00-b711c9bb1f17','','','2018-10-09 10:05:57','2018-10-09 14:05:57','',0,'http://local.hasteandhustle.com/8bf22a34-1e9e-4b51-be00-b711c9bb1f17/',0,'customize_changeset','',0),
	(103,1,'2018-10-09 10:06:12','2018-10-09 14:06:12','{\n    \"hastehustle::understrap_container_type\": {\n        \"value\": \"container\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-10-09 14:06:12\"\n    }\n}','','','trash','closed','closed','','88476efe-7d95-4af3-8bb1-76c4b78e817e','','','2018-10-09 10:06:12','2018-10-09 14:06:12','',0,'http://local.hasteandhustle.com/88476efe-7d95-4af3-8bb1-76c4b78e817e/',0,'customize_changeset','',0),
	(104,1,'2018-10-10 10:35:31','0000-00-00 00:00:00','','Auto Draft','','auto-draft','closed','closed','','','','','2018-10-10 10:35:31','0000-00-00 00:00:00','',0,'http://local.hasteandhustle.com/?post_type=acf-field-group&p=104',0,'acf-field-group','',0),
	(105,1,'2018-10-10 10:49:38','2018-10-10 14:49:38','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:4:\"left\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','Single Post Template','single-post-template','publish','closed','closed','','group_5bbe11717dd63','','','2018-10-11 17:17:39','2018-10-11 21:17:39','',0,'http://local.hasteandhustle.com/?post_type=acf-field-group&#038;p=105',0,'acf-field-group','',0),
	(106,1,'2018-10-10 10:53:20','2018-10-10 14:53:20','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:37:\"Enter the first content section here.\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}','First Content Section','first_content','publish','closed','closed','','field_5bbe11866beeb','','','2018-10-11 17:17:39','2018-10-11 21:17:39','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=106',1,'acf-field','',0),
	(107,1,'2018-10-10 10:54:10','2018-10-10 14:54:10','a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:24:\"Enter post subtitle here\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";i:200;s:4:\"rows\";i:3;s:9:\"new_lines\";s:0:\"\";}','Subtitle','subtitle','publish','closed','closed','','field_5bbe1264a349b','','','2018-10-10 13:40:42','2018-10-10 17:40:42','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=107',0,'acf-field','',0),
	(108,1,'2018-10-10 11:04:23','2018-10-10 15:04:23','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:27:\"Select the background image\";s:8:\"required\";i:1;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:19:\"jpg, png, webp, gif\";s:17:\"conditional_logic\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_5bbe145726aaf\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}}}','Page Break / Callout Section Background Image','break_callout_img','publish','closed','closed','','field_5bbe129826aad','','','2018-10-10 11:05:19','2018-10-10 15:05:19','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=108',3,'acf-field','',0),
	(109,1,'2018-10-10 11:04:23','2018-10-10 15:04:23','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;s:17:\"conditional_logic\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_5bbe145726aaf\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}}}','Page Break / Callout Section Content','break_callout_content','publish','closed','closed','','field_5bbe139626aae','','','2018-10-10 11:40:51','2018-10-10 15:40:51','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=109',4,'acf-field','',0),
	(110,1,'2018-10-10 11:04:23','2018-10-10 15:04:23','a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:39:\"Would you like to use a callout section\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:0;s:2:\"ui\";i:1;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}','Use Page Break / Callout Section','break_callout_choice','publish','closed','closed','','field_5bbe145726aaf','','','2018-10-10 11:37:08','2018-10-10 15:37:08','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=110',2,'acf-field','',0),
	(111,1,'2018-10-10 11:15:18','2018-10-10 15:15:18','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:31:\"Enter second content block here\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}','Second Content','second_content','publish','closed','closed','','field_5bbe1536f7394','','','2018-10-11 17:17:39','2018-10-11 21:17:39','',105,'http://local.hasteandhustle.com/?post_type=acf-field&#038;p=111',5,'acf-field','',0),
	(112,1,'2018-10-10 11:23:33','2018-10-10 15:23:33','a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:2:\"br\";}','Block Quote','block_quote','publish','closed','closed','','field_5bbe194f2bc82','','','2018-10-10 11:23:33','2018-10-10 15:23:33','',105,'http://local.hasteandhustle.com/?post_type=acf-field&p=112',6,'acf-field','',0),
	(113,1,'2018-10-10 13:45:18','2018-10-10 17:45:18','','Our Top 3 Reasons To Attend Generation Now','','inherit','closed','closed','','58-revision-v1','','','2018-10-10 13:45:18','2018-10-10 17:45:18','',58,'http://local.hasteandhustle.com/58-revision-v1/',0,'revision','',0),
	(114,1,'2018-10-11 13:11:03','2018-10-11 17:11:03','','callout-bkg','','inherit','open','closed','','callout-bkg','','','2018-10-11 13:11:03','2018-10-11 17:11:03','',58,'http://local.hasteandhustle.com/wp-content/uploads/2018/09/callout-bkg.jpg',0,'attachment','image/jpeg',0),
	(115,1,'2018-10-11 13:12:14','2018-10-11 17:12:14','','Our Top 3 Reasons To Attend Generation Now','','inherit','closed','closed','','58-revision-v1','','','2018-10-11 13:12:14','2018-10-11 17:12:14','',58,'http://local.hasteandhustle.com/58-revision-v1/',0,'revision','',0),
	(116,1,'2018-10-11 16:32:40','2018-10-11 20:32:40','','Our Top 3 Reasons To Attend Generation Now','','inherit','closed','closed','','58-revision-v1','','','2018-10-11 16:32:40','2018-10-11 20:32:40','',58,'http://local.hasteandhustle.com/58-revision-v1/',0,'revision','',0),
	(117,1,'2018-10-11 16:38:07','2018-10-11 20:38:07','','Our Top 3 Reasons To Attend Generation Now','','inherit','closed','closed','','58-revision-v1','','','2018-10-11 16:38:07','2018-10-11 20:38:07','',58,'http://local.hasteandhustle.com/58-revision-v1/',0,'revision','',0),
	(118,1,'2018-10-11 17:21:37','2018-10-11 21:21:37',' ','','','publish','closed','closed','','118','','','2018-10-11 17:21:37','2018-10-11 21:21:37','',0,'http://local.hasteandhustle.com/?p=118',2,'nav_menu_item','',0);

/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_term_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_relationships`;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`)
VALUES
	(1,1,0),
	(45,3,0),
	(45,8,0),
	(48,4,0),
	(48,9,0),
	(53,3,0),
	(58,3,0),
	(58,4,0),
	(58,5,0),
	(58,6,0),
	(58,7,0),
	(67,10,0),
	(69,10,0),
	(75,2,0),
	(78,2,0),
	(92,2,0),
	(93,2,0),
	(94,2,0),
	(95,2,0),
	(96,2,0),
	(99,2,0),
	(118,10,0);

/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_term_taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_taxonomy`;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`)
VALUES
	(1,1,'category','',0,0),
	(2,2,'nav_menu','',0,8),
	(3,3,'category','',11,3),
	(4,4,'category','',11,2),
	(5,5,'category','',11,1),
	(6,6,'category','',11,1),
	(7,7,'category','',0,1),
	(8,8,'category','',0,1),
	(9,9,'category','',0,1),
	(10,10,'nav_menu','',0,3),
	(11,11,'category','',0,0),
	(12,12,'category','',0,0);

/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_termmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_termmeta`;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;



# Dump of table wp_terms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_terms`;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`)
VALUES
	(1,'Uncategorised','uncategorized',0),
	(2,'Main Menu','main-menu',0),
	(3,'Startup','startup',0),
	(4,'Accelerate','accelerate',0),
	(5,'Growth','growth',0),
	(6,'Sell / IPO','sell-ipo',0),
	(7,'Resources','resources',0),
	(8,'Main Hero','main-hero',0),
	(9,'Secondary Hero','secondary-hero',0),
	(10,'Secondary Footer Menu','secondary-footer-menu',0),
	(11,'Business Stages','business-stages',0),
	(12,'Interviews','interviews',0);

/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_usermeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_usermeta`;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`)
VALUES
	(1,1,'nickname','kleurvision'),
	(2,1,'first_name','Ted'),
	(3,1,'last_name','Brogan'),
	(4,1,'description','CEO at <a href=\"#\">Bright Smiles Inc</a>'),
	(5,1,'rich_editing','true'),
	(6,1,'syntax_highlighting','true'),
	(7,1,'comment_shortcuts','false'),
	(8,1,'admin_color','fresh'),
	(9,1,'use_ssl','0'),
	(10,1,'show_admin_bar_front','true'),
	(11,1,'locale',''),
	(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),
	(13,1,'wp_user_level','10'),
	(14,1,'dismissed_wp_pointers','wp496_privacy'),
	(15,1,'show_welcome_panel','1'),
	(16,1,'session_tokens','a:4:{s:64:\"ee8deab8cf88bfed825de33b389c4b141d994fc1be0220998f62f12497dfa7e3\";a:4:{s:10:\"expiration\";i:1539787200;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1538577600;}s:64:\"5ad6fec29dde58ce0bc522b9e9199eb60afb8230ae868637803d59a9ae6f30e5\";a:4:{s:10:\"expiration\";i:1539354855;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1539182055;}s:64:\"1bcc22120b4f0cb536df0bb9e406f58f9256cefdac8d4c1eaa4656f9381235ac\";a:4:{s:10:\"expiration\";i:1539354862;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1539182062;}s:64:\"76be90d8b457b74b2e14bbbcc2a70c1151bf9dbd4a537018fcf7ba2271a58a85\";a:4:{s:10:\"expiration\";i:1540391667;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1539182067;}}'),
	(17,1,'wp_dashboard_quick_press_last_post_id','79'),
	(18,1,'wp_user-settings','hidetb=0&editor=tinymce&libraryContent=browse'),
	(19,1,'wp_user-settings-time','1536783103'),
	(20,1,'community-events-location','a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
	(21,1,'managenav-menuscolumnshidden','a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
	(22,1,'metaboxhidden_nav-menus','a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
	(23,1,'nav_menu_recently_edited','10'),
	(24,1,'wp_user_avatar','51'),
	(25,1,'closedpostboxes_post','a:0:{}'),
	(26,1,'metaboxhidden_post','a:6:{i:0;s:12:\"revisionsdiv\";i:1;s:11:\"postexcerpt\";i:2;s:13:\"trackbacksdiv\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";}');

/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_users`;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
VALUES
	(1,'kleurvision','$P$Bsa7e7ITWwEM87N3CAuMLhq45LyV6l1','kleurvision','omari@kleurvision.com','','2018-09-12 18:37:43','',0,'Ted Brogan');

/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
