# Translation of Filter Bar in Arabic
# This file is distributed under the same license as the Filter Bar package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-12-02 15:45:17+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: GlotPress/2.3.1\n"
"Language: ar\n"
"Project-Id-Version: Filter Bar\n"

#: src/Tribe/View.php:538
msgid "Notice: Utilizing the form controls will dynamically update the content"
msgstr ""

#: src/Tribe/View.php:537
msgid "Accessibility Form Notice"
msgstr ""

#: src/Tribe/Settings.php:161
msgid " (Note: When Live Refresh is enabled, TEC functionality may not be fully compliant with Web Accessibility Standards)"
msgstr ""

#: src/Tribe/Filter.php:367
msgctxt "Featured Events active filter display label"
msgid "Active"
msgstr ""

#: the-events-calendar-filter-view.php:173
msgid "To begin using The Events Calendar: Filter Bar, please install the latest version of"
msgstr ""

#: the-events-calendar-filter-view.php:52
msgid "Events Filter Bar"
msgstr ""

#: src/functions/php-min-version.php:76
msgid "Contact your Host or your system administrator and ask to upgrade to the latest version of PHP."
msgstr ""

#: src/functions/php-min-version.php:74
msgid "To allow better control over dates, advanced security improvements and performance gain."
msgstr ""

#: src/functions/php-min-version.php:64
msgid "<b>%1$s</b> requires <b>PHP %2$s</b> or higher."
msgid_plural "<b>%1$s</b> require <b>PHP %2$s</b> or higher."
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""
msgstr[5] ""

#: src/functions/php-min-version.php:52
msgctxt "Plugin A\",\" Plugin B"
msgid ", "
msgstr ""

#: src/functions/php-min-version.php:51
msgctxt "Plugin A \"and\" Plugin B"
msgid " and "
msgstr ""

#: src/Tribe/Settings.php:129
msgid "Show on initial page load"
msgstr ""

#: src/Tribe/Settings.php:128
msgid "Stay collapsed until visitors open it"
msgstr ""

#: src/Tribe/Settings.php:125
msgid "Filter Bar default state"
msgstr ""

#: src/Tribe/Filter.php:648
msgid "Multi-Select"
msgstr ""

#: src/Tribe/View.php:220
msgctxt "Featured Events active fitler display label"
msgid "Active"
msgstr ""

#: src/Tribe/View.php:427
msgid "Featured Events"
msgstr ""

#: src/Tribe/View.php:402
msgid "Organizers"
msgstr ""

#: src/Tribe/View.php:398
msgid "Venues"
msgstr ""

#: src/Tribe/View.php:383
msgid "Event Category"
msgstr ""

#: src/Tribe/Filters/Featured_Events.php:43
msgid "Show featured events only"
msgstr ""

#: src/Tribe/View.php:422
msgid "State/Province"
msgstr "الدولة / مقاطعة"

#: src/Tribe/View.php:418
msgid "City"
msgstr "المدينة"

#: src/Tribe/View.php:414
msgid "Country"
msgstr "البلد"

#: src/Tribe/Filters/Cost.php:101
msgid "Other"
msgstr "غير ذلك"

#: src/Tribe/Filters/Cost.php:89
msgctxt "Used as the identifier for free Events"
msgid "Free"
msgstr "مجانية"

#: src/Tribe/Filters/Cost.php:75
msgid "Used as the identifier for free Events"
msgstr "تستخدم كمعرف للأحداث المجانية"

#: src/Tribe/Filters/Cost.php:74
msgid "\"%s\" or empty or set to zero"
msgstr "\"%s\" البيانات المدخلة فارغة"

#: src/Tribe/Filters/Additional_Field.php:78
msgctxt "additional fields filter logic setting"
msgid "Logic:"
msgstr "المنطق"

#: src/Tribe/Filters/Additional_Field.php:74
msgctxt "additional fields filter logic setting"
msgid "Match any"
msgstr "تطابق أي"

#: src/Tribe/Filters/Additional_Field.php:68
msgctxt "additional fields filter logic setting"
msgid "Match all"
msgstr "كافة البيانات"

#. Author URI of the plugin/theme
msgid "http://m.tri.be/25"
msgstr "http://oncloudcalender.com"

#. Author of the plugin/theme
msgid "Modern Tribe, Inc."
msgstr "الشركة الحديثة"

#. Description of the plugin/theme
msgid "Creates an advanced filter panel on the frontend of your events list views."
msgstr "أنشاء لوحة تصفية متقدمة على الواجهة في وجهات العرض وقائمة الأحداث الخاصة بك."

#. Plugin Name of the plugin/theme
msgid "The Events Calendar: Filter Bar"
msgstr "أحداث التقويم: قائمة البحث"

#: the-events-calendar-filter-view.php:175
#: the-events-calendar-filter-view.php:176
msgid "The Events Calendar"
msgstr "تقويم الفعاليات"

#: src/views/filter-bar/filter-view-horizontal.php:38
#: src/views/filter-bar/filter-view-vertical.php:37
msgid "Submit"
msgstr "عرض"

#: src/views/filter-bar/filter-view-horizontal.php:29
#: src/views/filter-bar/filter-view-horizontal.php:44
#: src/views/filter-bar/filter-view-vertical.php:39
#: src/views/filter-bar/filter-view-vertical.php:43
msgid "Reset Filters"
msgstr "إعادة تعيين البحث"

#: src/views/filter-bar/filter-view-horizontal.php:28
#: src/views/filter-bar/filter-view-horizontal.php:43
#: src/views/filter-bar/filter-view-vertical.php:26
msgid "Show Filters"
msgstr "أظهار البحث"

#: src/views/filter-bar/filter-view-horizontal.php:27
#: src/views/filter-bar/filter-view-horizontal.php:42
#: src/views/filter-bar/filter-view-vertical.php:25
#: src/views/filter-bar/filter-view-vertical.php:42
msgid "Collapse Filters"
msgstr ""

#: src/views/filter-bar/filter-view-horizontal.php:27
#: src/views/filter-bar/filter-view-horizontal.php:42
#: src/views/filter-bar/filter-view-vertical.php:25
msgid "Show Advanced Filters"
msgstr ""

#: src/views/filter-bar/filter-view-horizontal.php:25
#: src/views/filter-bar/filter-view-vertical.php:29
msgid "Narrow Your Results"
msgstr ""

#: src/admin-views/settings-field-available-filters.php:8
msgid "Available Filters"
msgstr ""

#: src/admin-views/settings-field-active-filters.php:7
msgid "Active Filters"
msgstr ""

#: src/Tribe/View.php:410
msgid "Time"
msgstr ""

#: src/Tribe/View.php:406
msgid "Day"
msgstr ""

#: src/Tribe/View.php:394
msgid "Tags"
msgstr ""

#: src/Tribe/View.php:388
msgid "Cost (%s)"
msgstr ""

#: src/Tribe/Settings.php:152
msgid "Filter Bar"
msgstr ""

#: src/Tribe/Settings.php:119
msgid "Horizontal"
msgstr ""

#: src/Tribe/Settings.php:118
msgid "Vertical"
msgstr ""

#: src/Tribe/Settings.php:115
msgid "Filters Layout"
msgstr ""

#: src/Tribe/Settings.php:103
msgid "Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio)."
msgstr ""

#: src/Tribe/Settings.php:102
msgid "The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them."
msgstr ""

#: src/Tribe/Settings.php:40 src/Tribe/Settings.php:98
msgid "Filters"
msgstr ""

#: src/Tribe/Filters/Time_Of_Day.php:16
msgid "Night"
msgstr ""

#: src/Tribe/Filters/Time_Of_Day.php:15
msgid "Evening"
msgstr ""

#: src/Tribe/Filters/Time_Of_Day.php:14
msgid "Afternoon"
msgstr ""

#: src/Tribe/Filters/Time_Of_Day.php:13
msgid "Morning"
msgstr ""

#: src/Tribe/Filters/Time_Of_Day.php:12
msgid "All Day"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:20
msgid "Saturday"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:19
msgid "Friday"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:18
msgid "Thursday"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:17
msgid "Wednesday"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:16
msgid "Tuesday"
msgstr ""

#: src/Tribe/Filters/Day_Of_Week.php:15
msgid "Monday"
msgstr "الاثنين"

#: src/Tribe/Filters/Day_Of_Week.php:14
msgid "Sunday"
msgstr "السبت"

#: src/Tribe/Filters/Cost.php:105
msgid "Free"
msgstr "مجاني"

#: src/Tribe/Filters/Cost.php:81
msgid "Only when set to zero"
msgstr "فقط عند تعيينها إلى الصفر"

#: src/Tribe/Filters/Cost.php:69
msgid "Events are considered free when cost field is: %s %s"
msgstr "تعتبر أحداث مجانا عند حقل التكلفة: %s% s"

#: src/Tribe/Filters/Cost.php:59
msgid "Range Slider"
msgstr "قائمة الاختيار"

#: src/Tribe/Filters/Cost.php:55
msgid "Type: %s %s"
msgstr "النوع: %s% s"

#: src/Tribe/Filter.php:643 src/Tribe/Filters/Cost.php:64
msgid "Checkboxes"
msgstr "مربعات الاختيار"

#: src/Tribe/Filter.php:638
msgid "Dropdown"
msgstr "القائمة المنسدلة"

#: src/Tribe/Filter.php:634
msgid "Type: %s %s %s"
msgstr "النوع: %s% s% s"

#: src/Tribe/Filter.php:616
msgid "Title: %s"
msgstr "العنوان: %s"

#: src/Tribe/Filter.php:346
msgid "Select an Item"
msgstr "اختر عنصر"

#: src/Tribe/Filter.php:311
msgid "Select"
msgstr "اختيار"