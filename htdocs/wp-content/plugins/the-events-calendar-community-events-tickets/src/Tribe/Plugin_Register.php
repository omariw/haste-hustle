<?php

/**
 * Class Tribe__Events__Community__Tickets__Plugin_Register
 *
 * @since 4.6
 */
class  Tribe__Events__Community__Tickets__Plugin_Register extends Tribe__Abstract_Plugin_Register {

	protected $main_class   = 'Tribe__Events__Community__Tickets__Main';
	protected $dependencies = array(
		'parent-dependencies' => array(
			'Tribe__Events__Main'            => '4.8-dev',
			'Tribe__Tickets__Main'           => '4.10-dev',
		),
		'co-dependencies' => array(
			'Tribe__Tickets_Plus__Main'      => '4.10-dev',
			'Tribe__Events__Community__Main' => '4.6-dev',
		),
	);

	/**
	 * Constructor method.
	 *
	 * @since 4.6
	 */
	public function __construct() {
		$this->base_dir = EVENTS_COMMUNITY_TICKETS_FILE;
		$this->version  = Tribe__Events__Community__Tickets__Main::VERSION;

		$this->register_plugin();
	}
}
