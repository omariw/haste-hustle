<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">
                        
                        <img class="mx-auto d-block" src="<?php bloginfo('stylesheet_directory'); ?>/img/footer-logo.png" />
                        
                        <div class="social-media-footer">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                        
                        <div class="footer-menu">
                            <!-- The WordPress Menu goes here -->
                            <nav class="navbar navbar-expand-lg navbar-dark d-none d-lg-block">
                                <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <?php wp_nav_menu(
                                    array(
                                        'menu'            => 'Main Menu',
                                        'container_class' => 'collapse navbar-collapse',
                                        'container_id'    => 'navbarNavDropdown',
                                        'menu_class'      => 'navbar-nav',
                                        'menu_id'         => 'main-menu',
                                        'depth'           => 2,
                                        'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
                                    )
                                ); ?>
                            </nav>
                        </div>
                        
                        <div class="sec-footer-menu">
                            <?php wp_nav_menu(array( 'menu' => 'Secondary Footer Menu')); ?>
                        </div>
						<?php //understrap_site_info(); ?>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

