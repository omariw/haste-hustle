<?php
/**
 * Template Name: Conference Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container      = get_theme_mod( 'understrap_container_type' );
$upload_dir     = wp_upload_dir();

$conf_title     = get_field( "conference_title" );
$conf_date      = get_field( "conference_date" );
$conf_price     = get_field( "conference_price" );
?>

<style>
	
	.mobile-only{
		display: none;
	}
	.row.past-speakers-title{
	padding: 20px 0;
}

.sponsors-title h3, .row.past-speakers-title h3{
	text-align: center;
	font-size: 2em;	
}

.row.past-speakers{
	margin: 0px;
	padding: 20px 0;
}

.conference-title h2{
	font-family: vinyl;
	text-transform: uppercase;
}

.conference-title a:hover{
	text-decoration: none;
	
}

.conference-title a h1{
	border-bottom: 10px solid transparent;
}

.conference-title a:hover h1{
}

#branson-hero-bkg{
	width: 100%;
}

span.price{
	color: #000!important;
}

.conference-info h2{
	font-size: 2em!important;
}

.conference-title h1{
	max-width: 640px;
}

.conference-hero-wrapper .hero-left-side{
	padding: 8em 0;
}

h3.conf-info-title{
		padding-bottom: 0px!important;
}

@media screen and (max-width: 767px){
	.hero-row{
		background-color: #ff0058!important;
		background-image: none!important;
	}
	.conference-info h2{
		text-align: center;
	}
	.join-guest-list{
		display: block;
		padding: 20px 100px;
	}
	.join-guest-list a{
		display: block;
		text-align: center;
		margin-bottom: 10px;
	}
	
	.mobile-only{
		display: block;
	}
	
	.img-circle{
		width: 33%;
		border-radius: 50%;
		overflow: hidden;
		margin: 0 auto 20px;
		text-align: center;
	}
	
	.conference-title h1{
		margin: 0 auto;
	}
	
	

}

div.conference-wrapper{
	overflow-x: hidden;
}

h5.speaker-tag{
	font-size: 0.8em;
    color: #fff;
    font-family: vinyl;
    text-transform: uppercase;
    text-align: center;
    height: 50px;
}

.conference-speakers .other-speakers{
	margin-top: 30px;
}

.sponsor-wrapper{
	background: #fff;
}

.current-sponsors-2{
	margin-bottom: 60px;
}

.sponsors-title{
	padding-top: 40px;
}

.current-sponsors-1 img{
	padding: 0px 40px;
}

</style>

<div class="conference-wrapper" id="page-wrapper">
     <div class="conference-hero-wrapper">
        <div class="container-fluid" id="content">
	      
            <div class="hero-row row" style="background: url(<?= $upload_dir['baseurl']; ?>/2019/04/branson_conference_hero.jpg) no-repeat top right; background-size: cover;">
                <div class="hero-left-side col-lg-8">
                   <div class="row">
                        <div class="offset-md-2  col-sm-12 col-md-10 align-items-center">
                            <div class="conference-title">
	                            <img class="mobile-only img-circle" id="branson-circle" src="<?= $upload_dir['baseurl']; ?>/2019/04/branson_square_mobile.jpg" alt="Richard Branson Live in Tornto June 10 2019"/>
                                <a href="https://www.eventbrite.ca/e/richard-branson-live-haste-and-hustle-2020-tickets-60715053359?aff=ebdssbeac" target="_blank">
                                <h2>Sir Richard Branson Live in Toronto</h2>
                                <h1>Haste and Hustle</h1>
                                <h2><?php echo $conf_date; ?></h2>
                                </a>
                            </div>
                            <div class="conference-info">
                                <h2><!--<span>Date:</span> <span class="price"><?// $conf_date ?></span><br/>--><span>Tickets:</span> <span class="price"><?= $conf_price ?></span></h2>
                                
                                <h3 class="get-tickets-title">Get your tickets here</h3>
                                <a class="buy-tickets-btn" href="https://www.eventbrite.ca/e/richard-branson-live-haste-and-hustle-2020-tickets-60715053359?aff=ebdssbeac" target="_blank">Buy Tickets</a>
                                <p class="ticket-disclaimer">**All 2019 ticket holders have full access to this event with their previous tickets, in fact you have been upgraded, check your spam folder if you haven’t seen the messages from us.**</p>
                                <!--<div class="join-guest-list">
                                    <!-- Begin Mailchimp Signup Form 
                                    <div id="mc_embed_signup">
                                    <form action="https://hasteandhustle.us1.list-manage.com/subscribe/post?u=e96af8039f2b03aeb921e4b3e&amp;id=0541b59bb4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll">

                                    <div class="mc-field-group">
                                        <label class="be-first">Be the first to get tickets!</label>
                                        <input type="email" value="" placeholder="Enter Email Address" name="EMAIL" class="required email" id="mce-EMAIL">
                                    </div>
                                        <div id="mce-responses" class="clear">
                                            <div class="response" id="mce-error-response" style="display:none"></div>
                                            <div class="response" id="mce-success-response" style="display:none"></div>
                                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e96af8039f2b03aeb921e4b3e_0541b59bb4" tabindex="-1" value=""></div>
                                        <div class="clear"><input type="submit" value="Join Waitlist" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                        </div>
                                    </form>
                                    </div>

                                    <!--End mc_embed_signup-->
                                    <!--<a href="//tickets.hasteandhustle.com" target="_blank">Learn More</a>-->
                                </div>
                       </div>
                    </div>
                </div>
                </div>
                
                <div class="microphone-burst" style="right: 0;">
                    <img src="<?= $upload_dir['baseurl']; ?>/2019/01/ConfHero-MicBurst.png" />
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="conference-info-wrapper">
    <div class="container-fluid">
        <div class="row current-conference">
            <div class="col-md-10 offset-md-1">
            <div id="conference-info" class="row conference-info align-items-center">
                <div class="offset-lg-2 col-lg-8">

                    <p class="bold-statement">Haste and Hustle is an event for entrepreneurs to truly connect with one another, get inspired and educated by speakers and get prepared to take on the next challenges of their business.</p>
                     <p><iframe width="100%" height="400px" src="https://www.youtube.com/embed/fXqGyZkKRJM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                     <h3 class="conf-info-title">Information:</h3>
                    <p>This is a super scrappy, fun and inspiring event that provides a disproportionate amount of value to each guest. In just two short years Haste and Hustle has brought together some of the most celebrated entrepreneurs in the industry and has grown nearly 175%.  Our goal now is to grow in depth and to truly connect with each guest and support their journey in a meaningful way.</p>
                </div>
            </div><!-- .row -->

            <div class="row conference-info align-items-center">
                <div class="offset-lg-2 col-lg-8">
                    <h3 class="conf-info-title">What It Is:</h3>
                    <p>In 2020 the roof is getting blown off this event with a lineup of speakers that is second to none.</p>
                    <p>Haste and Hustle’s number one mission is to serve and support entrepreneurs. We focus specifically on start-ups and mid-stage entrepreneurs.  We are here to educate, inspire and connect all our guests.</p>
                    <p>This is a scrappy, but deeply meaningful event for those that attend. We are building an incredible community and we not only WANT you to join, we NEED you. Our community is strengthened by all the talents, skills and networks that each of you bring to the table.</p>
                </div>
            </div><!-- .row -->

            <div class="row conference-info align-items-center">
                <div class="offset-lg-2 col-lg-8">
                    <h3 class="conf-info-title">Who Attends:</h3>
                    <p>We have been lucky to attract some pretty awesome guests to our past events.  These guests represent the following sectors:</p>
                        <p>• Technology (including blockchain, AI and nanotechnology)</p>
                        <p>• Banking, investment and Fintech</p>
                        <p>• Retail</p>
                        <p>• Sales (Real estate, mortgage, luxury goods)</p>
                        <p>• Insurance</p>
                </div>
            </div><!-- .row -->


            <div id="current-speakers" class="conference-speakers">
                <div class="current-speakers-wrapper-1">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-6 col-md-12">
                            <h3 class="speaker-title">2020 Speakers:</h3>
                        </div>
                    </div>
                    
                    <div class="row past-speakers other-speakers align-items-center">
                    <?php
                            $numOfCols = 3;
                            $rowCount = 0;
                            $bootstrapColWidth = 12 / $numOfCols;
                            $column_count = 0;
                            $args = array(
                                'category__in' => '126',
                                'posts_per_page'=> -1, // Number of related posts that will be shown.
                                'ignore_sticky_posts'=>1,
                                'order' => 'ASC' 
                            );
                        
                            $current_speakers = new WP_Query( $args );
                            
                            while ( $current_speakers->have_posts() ) : $current_speakers->the_post();
                                $current_speakers_permalink       = get_the_permalink($post->ID);
                                $current_speakers_author_first    = get_the_author_meta('user_firstname');
                                $current_speakers_author_last     = get_the_author_meta('user_lastname');
                                $column_count++;
                                if($column_count == '1') {
                                    $column_offset = 'offset-lg-2';
                                } else {
                                    $column_offset = 'offset-md-1';
                                }
                
                    ?>
                                <div class="<?= $column_offset; ?> col-lg-2 col-md-3 col-sm-4 other-speaker-info" data-toggle="tooltip" title="<?php the_excerpt(); ?>">
                                    <?php the_post_thumbnail('full'); ?>
                                    <h4 class="other-speaker-name"><?php the_title(); ?></h4>
                                    <h5 class="speaker-tag"><?php the_field('position_titles'); ?></h5>
                                    <a class="bio-read-link btn" href="<?php the_permalink(); ?>">Read Bio</a>
                                </div>
                                
                    <?php 
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) {
                                echo '</div><div class="row other-speakers align-items-center">';
                                $column_count = 0;
                            } 
                            endwhile;
                    ?>
                    </div> 
                    <!--<div class="row past-speakers other-speakers align-items-center">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/richard-branson-profile.jpg" />
                            <h4 class="other-speaker-name">Richard Branson</h4>
                            <h5 class="speaker-tag">Keynote Speaker & Founder at Virgin Group</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/02_Alex-Mashinsky.png" />
                            <h4 class="other-speaker-name">Alex Mashinsky</h4>
                            <h5 class="speaker-tag">CEO, Celsius Network</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/03_Fabio-Marrama.png" />
                            <h4 class="other-speaker-name">Fabio Marrama</h4>
                            <h5 class="speaker-tag">Director, Community Experience at Meridian Credit Union</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/04_david-rock.png" />
                            <h4 class="other-speaker-name">David Rock</h4>
                            <h5 class="speaker-tag">Videographer and Entrepreneur</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/05_Kelsey-Ramsden.png" />
                            <h4 class="other-speaker-name">Kelsey Ramsden</h4>
                            <h5 class="speaker-tag">President, Belvedere Place Development, Author, Speaker</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/06_scott-barker.png" />
                            <h4 class="other-speaker-name">Scott Barker</h4>
                            <h5 class="speaker-tag">President, Haste and Hustle</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/07_Fatima-Zaidi.png" />
                            <h4 class="other-speaker-name">Fatima Zaidi</h4>
                            <h5 class="speaker-tag">Co-Founder and CEO, Quill</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/08_John-Stix.png" />
                            <h4 class="other-speaker-name">John Stix</h4>
                            <h5 class="speaker-tag">Creator of “I’M IN” — Discover your WHO for business and individuals</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/09_Espree-Devora.png" />
                            <h4 class="other-speaker-name">Espree Devora</h4>
                            <h5 class="speaker-tag">Producer/Host, Women in Tech Podcast</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/10_Adrian-Salamunovic.png" />
                            <h4 class="other-speaker-name">Adrian Salamunovic</h4>
                            <h5 class="speaker-tag">Founder at Earned and Serial Entrepreneur</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/11_shauna-arnott.png" />
                            <h4 class="other-speaker-name">Shauna Arnott</h4>
                            <h5 class="speaker-tag">Founder and CEO, Haste and Hustle</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/12_George-Khalife.png" />
                            <h4 class="other-speaker-name">George Khalife</h4>
                            <h5 class="speaker-tag">Director, Sampford Advisors</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/13_Zara-Seyed.png" />
                            <h4 class="other-speaker-name">Zara Seyed</h4>
                            <h5 class="speaker-tag">Student, The Knowledge Society</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/14_Jim-Van-Eerden.png" />
                            <h4 class="other-speaker-name">Jim Van Eerden</h4>
                            <h5 class="speaker-tag">President of 5th Element Group</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/15_Anu-Bhardwaj.png" />
                            <h4 class="other-speaker-name">Anu Bhardwaj</h4>
                            <h5 class="speaker-tag">Founder, WIIN Digital</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center mb-5">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/16_Nesh-Pillay.png" />
                            <h4 class="other-speaker-name">Nesh Pillay</h4>
                            <h5 class="speaker-tag">Founder, Press Pillay</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/17_Swish-Goswami.png" />
                            <h4 class="other-speaker-name">Swish Goswami</h4>
                            <h5 class="speaker-tag">CEO, Trufan</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/18_Alex-Brown.png" />
                            <h4 class="other-speaker-name">Alex Brown</h4>
                            <h5 class="speaker-tag">E-Commerce Brand Builder</h5>
                        </div>
                    </div><!-- .row 
                    <div class="row other-speakers align-items-center mb-5">
                        <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/19_abu_sayed.png" />
                            <h4 class="other-speaker-name">Abu Sayed</h4>
                            <h5 class="speaker-tag">Founder, Wildhood</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/20_lizzie-pierce.png" />
                            <h4 class="other-speaker-name">Lizzie Peirce</h4>
                            <h5 class="speaker-tag">Producer, Know Hau Media</h5>
                        </div>
                        <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                            <img src="<?//= $upload_dir['baseurl']; ?>/speakers/21_trang.png" />
                            <h4 class="other-speaker-name">Trang Trinh</h4>
                            <h5 class="speaker-tag">Founding Director & CEO of TREC Brands</h5>
                        </div>
                    </div><!-- .row -->
                </div><!-- Container end -->
            </div>
        </div>
    </div>
</div><!-- container-fluid end -->
<div class="sponsor-wrapper container-fluid">
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="row sponsors-title">
                <div class="col-12">
                    <h3>2020 Haste & Hustle Sponsors and Partners</h3>
                </div>
            </div>
            <div class="row current-sponsors-1 main-sponsors">
                <div class="col-lg-6 offset-md-3 col-md-6">
                    <a href="https://www.meridiancu.ca/Small-Business.aspx" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/01_meridian.jpg" /></a>
                </div>
                <div class="col-lg-6 offset-md-3 col-md-6 mb-5">
                    <a href="https://www.trecbrands.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/brainworks-logo.png" /></a>
                </div>
            </div>
            <div class="row current-sponsors current-sponsors-2 align-items-center">
                <div class="col-md-3">
                    <a href="https://dmz.ryerson.ca" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/03_dmz.jpg" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://www.trufan.io" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/04_trufan.jpg" /></a>
                </div>
                 <div class="col-md-3">
                      <a href="https://eightyeightagency.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/05_eightyeight.jpg" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://celsius.network" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/06_celcius.jpg" /></a>
                </div>
            </div>
            <div class="row current-sponsors current-sponsors-3 align-items-center">
                <div class="col-md-3">
                    <a href="https://kswiss.ca" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/kswiss-logo.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://consandkernels.com/" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/cons-kernels.jpeg" /></a>
                </div>
                 <div class="col-md-3">
                      <a href="https://cheekbonebeauty.ca/" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/cheekbone-logo.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a class="nowcreative-size" href="https://www.nowcreativegroup.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/now-creative.jpg" /></a>
                </div>
            </div>
            <div class="row current-sponsors current-sponsors-4 align-items-center">
                <div class="col-md-3">
                    <img class="wiiw-size" src="<?= $upload_dir['baseurl']; ?>/sponsors/women-investing-in-women.jpeg" />
                </div>
                 <div class="col-md-3">
                     <img class="wiiw-size" src="<?= $upload_dir['baseurl']; ?>/sponsors/the-state-of-women.jpeg" />
                </div>
                 <div class="col-md-3">
                      <a href="https://www.givesome.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/givesome.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://brookfieldinstitute.ca" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/brookfield-institute.png" /></a>
                </div>
            </div>
            <div class="row current-sponsors current-sponsors-5 align-items-center">
                <div class="col-md-3">
                    <a href="https://startupheretoronto.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/startup-toronto.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://www.toronto.ca/" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/city-of-toronto-logo.jpg" /></a>
                </div>
                 <div class="col-md-3">
                      <img src="<?= $upload_dir['baseurl']; ?>/sponsors/qrypto-queens.jpeg" />
                </div>
                 <div class="col-md-3">
                     <a href="https://5thelement.group" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/fifth-element-group.png" /></a>
                </div>
            </div>
            <div class="row current-sponsors current-sponsors-5 align-items-center">
                <div class="col-md-3">
                    <a href="http://www.northernhci.ca" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/tractus-logo.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://www.niagaracollege.ca" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/niagara-college.png" /></a>
                </div>
                 <div class="col-md-3">
                      <a href="https://startwell.co" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/startwell-logo.png" /></a>
                </div>
                 <div class="col-md-3">
                     <a href="https://office146.com" target="_blank"><img src="<?= $upload_dir['baseurl']; ?>/sponsors/office146_logo.png" /></a>
                </div>
            </div>
        </div>
    </div>
	
</div>
    <div class="conference-speakers past-conference-wrapper">
        <div class="container-fluid">
            <div class="row">
                <a data-toggle="collapse" class="past-speakers-dropdown" href="#past-speakers-collapse" role="button" aria-expanded="false" aria-controls="past-speakers-collapse">
                    <div class="offset-lg-2 col-lg-6 col-sm-12">
                        <h3 class="speaker-title">Past Speakers: <i class="fa fa-chevron-down section-arrow" aria-hidden="true"></i></h3>
                    </div>
                </a>
            </div>
            <div id="past-speakers-collapse" class="past-speakers-wrapper collapse">
                <div class="row past-speakers other-speakers align-items-center">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/majit.jpg" />
                        <h4 class="other-speaker-name">Manjit Minhas</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/allie.jpg" />
                        <h4 class="other-speaker-name">Allie Hughes</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/casey.jpg" />
                        <h4 class="other-speaker-name">Casey Neistat</h4>
                    </div>
                </div><!-- .row -->
                <div class="row other-speakers align-items-center">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/barney.jpg" />
                        <h4 class="other-speaker-name">Barney Waters</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/marc.jpg" />
                        <h4 class="other-speaker-name">Marc Saltzman</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/GV.jpg" />
                        <h4 class="other-speaker-name">Gary Vaynerchuk</h4>
                    </div>
                </div><!-- .row -->
                <div class="row other-speakers align-items-center">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/anthony.jpg" />
                        <h4 class="other-speaker-name">Anthony Ricciardi</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/amanda.jpg" />
                        <h4 class="other-speaker-name">Amanda Robinson</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/rg.jpg" />
                        <h4 class="other-speaker-name">Erin Bury</h4>
                    </div>
                </div><!-- .row -->
                <div class="row other-speakers align-items-center">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/peter.jpg" />
                        <h4 class="other-speaker-name">Peter Maoloni</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/manu.jpg" />
                        <h4 class="other-speaker-name">Manu (Swish) Goswami</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/kim.jpg" />
                        <h4 class="other-speaker-name">Kim Parnell</h4>
                    </div>
                </div><!-- .row -->
                <div class="row other-speakers align-items-center">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/ian.jpg" />
                        <h4 class="other-speaker-name">Ian Szabo</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/fatima.jpg" />
                        <h4 class="other-speaker-name">Fatima Zaidi</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/eli.jpg" />
                        <h4 class="other-speaker-name">Eli Brown</h4>
                    </div>
                </div><!-- .row -->
                <div class="row other-speakers align-items-center mb-5">
                    <div class="offset-lg-2 col-lg-2 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/connor.jpg" />
                        <h4 class="other-speaker-name">Connor Beaton</h4>
                    </div>
                    <div class="col-lg-2 offset-md-1 col-md-3 col-sm-4 other-speaker-info">
                        <img src="<?= $upload_dir['baseurl']; ?>/2019/01/bobby.jpg" />
                        <h4 class="other-speaker-name">Bobby Umar</h4>
                    </div>
                </div><!-- .row -->
            </div>
            
            <div class="row">
                <a data-toggle="collapse" class="past-sponsor-dropdown" href="#past-sponsor-collapse" role="button" aria-expanded="false" aria-controls="past-sponsor-collapse">
                    <div class="offset-lg-2 col-lg-6 col-sm-12">
                        <h3 class="speaker-title">Past Sponsors: <i class="fa fa-chevron-down section-arrow" aria-hidden="true"></i></h3>
                    </div>
                </a>
            </div>
            <div id="past-sponsor-collapse" class="past-sponsor-wrapper collapse">
                <div class="previous-sponsor-footer">
                    <div class="row">
                    <div class="offset-lg-2 col-lg-8 col-md-12">
                    <div class="row past-sponsors other-speakers">
                        <div class="col-lg-6 col-md-6">
                            <img src="<?= $upload_dir['baseurl']; ?>/2019/01/hh_sponsors_2018.jpg" />
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <img src="<?= $upload_dir['baseurl']; ?>/2019/01/hh_sponsors_2017.jpg" />
                        </div>
                    </div><!-- .row -->
                    </div>
                    </div>
                </div>
            </div>
        </div><!-- Container end -->
    </div>
</div><!-- Wrapper end -->


<div class="sponsor-volunteer-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 sponsor-column">
                <div class="row">
                    <div class="offset-md-2 col-lg-8 col-md-8">
                        <h3>Sponsors:</h3>
                        <p>Haste and Hustle sponsors get an assortment of amazing opportunities to reach and interact with the amazing audience.  We love to sit down with each sponsor and find out how we can support your goals and help you realize a great opportunity for your brand/business at Haste and Hustle.</p>

                        <!--<p class="small-text">Join our guest list to receive our sponsorship deck.</p>-->
                        <div class="join-guest-list">
                            <a href="mailto:Doug@hasteandhustle.com?subject=Sponsors%20Info">Get More Info</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 volunteer-column">
                <div class="row">
                    <div class="offset-md-2 col-lg-8 col-md-8">
                        <h3>Volunteers:</h3>
                        <p>This year the volunteer division has gotten a whole lot cooler.  We are developing a Haste and Hustle Squad to support the needs of all our guests.  This opportunity allows you to get right into the heart of the action.  If you have a desire to serve others, make an impact, have some fun and meet great people please sign up to join us as we grow Haste and Hustle in 2019.</p>
                        
                        <div class="join-guest-list">
                            <a href="mailto:crew@hasteandhustle.com?subject=Join%20Volunteer%20Squad">Join the Squad Today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	fbq('track', 'ViewContent');
</script>

<?php get_footer(); ?>
