<?php
/**
 * Template Name: Sponsorship Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container      = get_theme_mod( 'understrap_container_type' );
$upload_dir     = wp_upload_dir();

$conf_title     = get_field( "conference_title" );
$conf_date      = get_field( "conference_date" );
$conf_price     = get_field( "conference_price" );
?>

<style>
	
	.mobile-only{
		display: none;
	}
	.row.past-speakers-title{
	padding: 20px 0;
}

.sponsors-title h3, .row.past-speakers-title h3{
	text-align: center;
	font-size: 2em;	
}
    
.conference-title h2{
	font-family: vinyl;
	text-transform: uppercase;
}

.conference-title a:hover{
	text-decoration: none;
	
}

.conference-title a h1{
	border-bottom: 10px solid transparent;
}

.conference-title a:hover h1{
}

#branson-hero-bkg{
	width: 100%;
}

span.price{
	color: #000!important;
}

.conference-info h2{
	font-size: 2em!important;
}

.conference-title h1{
	font-size: 6em!important;
	max-width: 640px;
}

.conference-hero-wrapper .hero-left-side{
	padding: 8em 0;
}

h3.conf-info-title{
		padding-bottom: 0px!important;
}

@media screen and (max-width: 767px){
	.hero-row{
		background-color: #ff0058!important;
		background-image: none!important;
	}
	.conference-info h2{
		text-align: center;
	}
	.join-guest-list{
		display: block;
		padding: 20px 100px;
	}
	.join-guest-list a{
		display: block;
		text-align: center;
		margin-bottom: 10px;
	}
	
	.mobile-only{
		display: block;
	}
	
	.img-circle{
		width: 33%;
		border-radius: 50%;
		overflow: hidden;
		margin: 0 auto 20px;
		text-align: center;
	}
	
	.conference-title h1{
		margin: 0 auto;
	}
	
	

}

div.conference-wrapper{
	overflow-x: hidden;
}

h5.speaker-tag{
	font-size: 0.8em;
    color: #fff;
    font-family: vinyl;
    text-transform: uppercase;
    text-align: center;
    height: 50px;
}

.conference-speakers .other-speakers{
	margin-top: 30px;
}

.sponsor-wrapper{
	background: #fff;
}

.current-sponsors-2{
	margin-bottom: 60px;
}


.current-sponsors img{
	width: 100%;
	padding: 0 20px;
}

.sponsors-title{
	padding-top: 40px;
}

.current-sponsors-1 img{
	padding: 0px 40px;
}

</style>

<div class="sponsorship-wrapper" id="page-wrapper">
     <div class="sponsorship-hero-wrapper">
        <div class="container-fluid" id="content">
	      
            <div class="row">
                <div class="hero-left-side col-lg-6">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-8 offset-md-2 col-md-8">
                            <div class="sponsorship-title">
                                <h1>Show your haste and hustle and become a sponsor</h1>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="hero-right-side col-lg-6">
                   <div class="row">
                        <div class="offset-lg-3 col-lg-8 offset-md-2 col-md-8">
                            <div class="sponsor-form">
                                <?php
                                    echo do_shortcode('[gravityform id=5 title=false description=false ajax=true tabindex=49]');
                                ?>
                            </div>
                       </div>
                    </div>
                </div>
                
                <div class="microphone-burst" style="right: 0;">
                    <img src="<?= $upload_dir['baseurl']; ?>/2019/01/ConfHero-MicBurst.png" />
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="conference-info-wrapper sponsorship-info-wrapper">
    <div class="container">
        <div class="row sponsorship-info align-items-center">
            <div class="offset-lg-2 col-lg-8 sponsorship-details">
                <h3 class="sponsor-info-title">Information:</h3>
                <p>Sponsorship at the Haste and Hustle Conference is an opportunity for brands to align themselves with a growing brand that reaches ambitious entrepreneurs every single day. Sponsors gain access to bright and driven entrepreneurs and other prospects who attend with the intent of learning skills to build their business and drive their success to new levels.</p>
                <p>This demographic is notoriously well researched and driven to work within a framework of trust.  The best way to gain the attention of these individuals at the start of their success is to meet them in person.  We invite you to sponsor Haste and Hustle to deepen those relationships with the most ambitious crowd in North America.
                </p>
            </div>
        </div><!-- .row -->

        <div class="row sponsorship-info align-items-center">
            <div class="offset-lg-2 col-lg-8">
                <h3 class="sponsor-info-title">Current Demographic</h3>
                <div class="row demographic-row">
                    <div class="col-lg-6 roles-titles">
                        <h4>Roles & Titles</h4>
                        <ul>
                            <li>Founders & CEO’s of Startups</li>
                            <li>Sales & Marketing professionals</li>
                            <li>Real estate, insurance, mortgage and technology</li>
                            <li>Investors and VC’s</li>
                            <li>Creators</li>
                            <li>Lawyers</li>
                            <li>Alternative health care professionals</li>
                            <li>Web & software developers</li>
                            <li>Business students</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 industry-sector">
                        <h4>Industry & Sector</h4>
                        <ul>
                            <li class="stat-item"><span>10%</span> Insurance</li>
                            <li class="stat-item"><span>20%</span> Real Estate</li>
                            <li class="stat-item"><span>20%</span> Technology</li>
                            <li class="stat-item"><span>5%</span> Retail Startups</li>
                            <li class="stat-item"><span>5%</span> Legal</li>
                            <li class="stat-item"><span>20%</span> Marketing/Communications</li>
                            <li class="stat-item"><span>20%</span> Creatives</li>
                        </ul>
                    </div>
                </div>
                <div class="row demographic-row">
                    <div class="col-lg-6 geographic-reach">
                        <h4>Attendee Geographic Reach</h4>
                        <ul>
                            <li class="stat-item"><span>55%</span> GTA</li>
                            <li class="stat-item"><span>25%</span> Ontario (outside of GTA)</li>
                            <li class="stat-item"><span>15%</span> From other parts of Canada and USA</li>
                            <li class="stat-item"><span>5%</span> Internationally</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 age-gender">
                        <h4>Age and Gender, 25-35</h4>
                        <ul>
                            <li class="stat-item"><span>60%</span> Men</li>
                            <li class="stat-item"><span>40%</span> Women</li>
                        </ul>
                    </div>
                </div>
                <div class="row demographic-row">
                    <div class="col-lg-6 social-media-reach">
                        <h4>Social Media Reach: <span>(Last 365 Days)</span></h4>
                        <ul>
                            <li class="stat-item"><span>380,000K +</span> Views</li>
                            <li class="stat-item"><span>6.5 million +</span> Minutes Watched</li>
                            <li class="stat-item"><span>4700 +</span> Subscribers</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 demographics">
                        <h4>Demographics</h4>
                        <ul>
                            <li class="stat-item"><span>44%</span> Women</li>
                            <li class="stat-item"><span>56%</span> Men</li>
                        </ul>
                    </div>
                </div>
                
                <div class="row demographic-row">
                    <div class="col-lg-12 footer-sponsor-form">
                        <h3 class="become-sponsor-title">For more information on how to <span>become a sponsor</span> submit information here:</h3>
                        <div class="sponsor-form">
                            <?php
                                echo do_shortcode('[gravityform id=5 title=false description=false ajax=true tabindex=49]');
                            ?>
                        </div>
                        
                    </div>
                </div>
                <h5 class="previous-sponsor-title">Your our in good company<br><span>our previous sponsors include:</span></h5>
            </div>
            
        </div><!-- .row -->
    </div>
</div><!-- Wrapper end -->
<div class="previous-sponsor-footer container">
	<div class="row past-sponsors other-speakers">
        <div class="col-lg-6 col-md-6">
            <img src="<?= $upload_dir['baseurl']; ?>/2019/01/hh_sponsors_2018.jpg" />
        </div>
        <div class="col-lg-6 col-md-6">
            <img src="<?= $upload_dir['baseurl']; ?>/2019/01/hh_sponsors_2017.jpg" />
        </div>
    </div><!-- .row -->
</div>


<script>
	fbq('track', 'ViewContent');
</script>

<?php get_footer(); ?>
