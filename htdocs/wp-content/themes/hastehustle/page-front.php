<?php
/**
 * Template Name: Front Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );

$args = array(
    'cat' 	  	        => array( 8 ),
    'posts_per_page'    => 1,
    'category__not_in'  => array(126, 27)
);

$query = new WP_Query( $args );

if ( $query->have_posts() ) {
    while ($query->have_posts()) {
        $query->the_post();
        $post_id        = $post->ID;
        $category       = get_the_category();
        $the_title      = get_the_title();
        $featured_image = get_the_post_thumbnail_url($post_id, 'full', array('class' => 'main-hero-img'));
        $author         = get_the_author();
        $permalink      = get_permalink($post_id);
        $author_avatar  = get_wp_user_avatar(get_the_author_meta('ID'), 'rounded-circle main-avatar', 'original');
        // print_r($author_meta);
        /* Search for featured post terminology */
        if($category) {
            foreach ($category as $key => $value) {
                if (strpos($value->slug, 'main-hero') !== false || strpos($value->slug, 'secondary-hero') !== false) {
                    unset($category[$key]);
                } else {
                    $category_name	= $category[$key]->name;
                    $main_post      = $post_id;
                    break;
                }
            }
        }
        
        // Get the ID of a given category
        $category_id = get_cat_ID( $category_name );
        // Get the URL of this category
        $category_link = get_category_link( $category_id );
        /*

        //Get the post permalink
        $permalink		= get_permalink($post_id);
        style="background:url('<?= $featured_image; ?>') no-repeat; background-size: cover;"
        */
    }
}

$upload_dir     = wp_upload_dir();
?>

<div class="wrapper home-wrapper" id="page-wrapper">

	<div class="container-fluid" id="content">

		<div class="row hero-post-wrapper">
            
            <div class="col-md-12 hero-post-column">
                        
                <div class="hero-post">
                    <img class="desktop-hero d-none d-sm-none d-md-block" src="<?= $upload_dir['baseurl']; ?>/2019/01/hh_home_main_image.jpg" />
                    <img class="mobile-hero d-md-none" src="<?= $upload_dir['baseurl']; ?>/2019/02/hh_home_main_mobile.jpg" />

                    <!--<div class="row hh_category">
                        <div class="col-md-12 post-category">
                            <h2><a href="<?//= $category_link; ?>"><?//= $category_name; ?></a></h2>
                        </div>
                    </div>
                    
                    <div class="row post-info">
                        <!--<div class="align-self-center author-info col-md-3">
                            <?//= $author_avatar; ?>
                            <p><?//= $author; ?></p>
                        </div>
                        <div class="post-title col-md-12">
                            <h1><a href="<?//= $permalink; ?>"><?//= $the_title; ?></a></h2>
                        </div>
                    </div>  -->
                    <div class="hero-tagline container">
                        <h1>Welcome to Haste and Hustle! The place where entrepreneurs connect & learn: Branding, Business Finance, Legal Strategies, Marketing, Healthy Living, Sales Strategies, Mindset Tricks, Success Strategies</h1>
                    </div>     
                </div> 
                    
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
        
			<div class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-7 col-lg-8 order-md-1 order-12<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">

				<main class="row site-main main-post-feed" id="main" role="main">
                    <div class="col-md-12">
                        
                        <div class="row main-feed-row">
                            <div class="latest-news">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/lastest-new-bkg.jpg" />
                                <h3>The Latest</h3>
                            </div>
                            <?php 
                                
                               $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                               global $wp_query;
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page'		=> '26',
                                    'paged' => $paged,
                                    'category__not_in'  => array(126, 27)
                                );
                                
                                //$posts = new WP_Query( array_merge($args, $wp_query->query) );
                                $posts = new WP_Query( $args);                                
                                $numOfCols = 2;
                                $rowCount = 0;
                                while ( $posts->have_posts() ) : $posts->the_post(); ?>

                                <?php 
                                    get_template_part( 'loop-templates/content', 'page' ); 
                                    $rowCount++;
                                    if($rowCount % $numOfCols == 0) echo '</div><div class="row main-feed-row">'; 
                                ?>

                                <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                /*if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;*/
                                ?>

                            <?php endwhile; // end of the loop.  ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                    if (function_exists("pagination")) {
                                      pagination($posts->max_num_pages);
                                    }
                                ?>
                                
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_template_part( 'sidebar-templates/sidebar', 'home' ); ?>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
