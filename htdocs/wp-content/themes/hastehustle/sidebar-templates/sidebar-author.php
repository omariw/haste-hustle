<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( 'right-sidebar' ) ) {
	return;
}

$author_id = get_the_author_meta('ID');

?>

<div class="sidebar-feed widget-area" id="right-sidebar" role="complementary">
    <div class="row">
        <div class="trending col-md-12">
            <div class="author-popular-sidebar">
                <?php 
                    
                    $popular = new WP_Query(array('author'=>$author_id, 'posts_per_page'=>3, 'meta_key'=>'popular_posts', 'orderby'=>'meta_value_num', 'order'=>'DESC'));
                    while ($popular->have_posts()) : $popular->the_post(); 
                
                    $post_id        = $post->ID;
                    $category       = get_the_category();
                    $the_title      = get_the_title();
                    $featured_image = get_the_post_thumbnail_url($post_id, 'full', array('class' => 'popular-home-img'));
                    $author         = get_the_author();
                    $permalink      = get_permalink($post_id);
                    $author_avatar  = get_wp_user_avatar(get_the_author_meta('ID'), 'rounded-circle main-avatar', 'original');
                    $post_date      = get_the_date();
                    print_r($author_meta);
                    /* Search for featured post terminology */
                    if($category) {
                        foreach ($category as $key => $value) {
                            if (strpos($value->slug, 'main-hero') !== false || strpos($value->slug, 'secondary-hero') !== false) {
                                unset($category[$key]);
                            } else {
                                $category_name	= $category[$key]->name;
                                break;
                            }
                        }
                    }

                    // Get the ID of a given category
                    $category_id = get_cat_ID( $category_name );
                    // Get the URL of this category
                    $category_link = get_category_link( $category_id ); ?>

                    <div class="hh-post">
                        <article <?php post_class('hh-contain'); ?> id="post-<?php the_ID(); ?>" style="background:url('<?= $featured_image; ?>') no-repeat; background-size: cover;">

                                <div class="hh_category">
                                    <h2><a href="<?= $category_link; ?>"><?= $category_name; ?></a></h2>
                                </div>

                        </article><!-- #post-## -->
                        <div class="post-info">
                            <div class="feed-post-title">
                                <h1><a href="<?= $permalink; ?>"><?= $the_title; ?></a></h2>
                                <hr>
                            </div>

                            <div class="feed-author-info">
                                <div class="row">
                                    <div class="d-flex author-avatar align-items-center col-2">
                                       <?= $author_avatar; ?>
                                    </div>
                                    <div class="d-flex author-name align-items-center col-5">
                                        <p><?= the_author_posts_link(); ?></p>
                                    </div>
                                    <div class="d-flex author-post-date align-items-center col-5">
                                        <p><?= $post_date; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php //dynamic_sidebar( 'right-sidebar' ); ?>

</div><!-- #right-sidebar -->
