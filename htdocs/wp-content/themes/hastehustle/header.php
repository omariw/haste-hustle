<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<!-- Sumo -->
	<script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='52ea6b6a53dc6a09b13506e4ee1c811e5ef8442f804ce7e879c4355255f70345';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script>
	<!-- End Sumo -->
	
	<!-- Global site tag (gtag.js) - Google Ads: 745509418 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-745509418"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	
	  gtag('config', 'AW-745509418');
	</script>
	<!-- End Google Tag -->
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '2366101173617288'); 
	fbq('track', 'PageView');
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=2366101173617288&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

	<?php wp_head(); ?>
	
	<style>
		
		
		body .tagline-wrapper{
			left: 250px;
		}
		
		.other-speaker-info img{
			width: 100%;
		}
		
		#current-speakers{
			padding-bottom: 40px;
			margin-top: 40px;
			padding-top: 40px;
		}
		
		@media screen and (min-width: 768px) and (max-width: 991px){
			body .tagline-wrapper{
				display: none;
			}
			
		}
		
		@media screen and (max-width: 767px){
			#wrapper-navbar .container{
				width: 100%;
				max-width: 100%;
			}
			body .tagline-wrapper{
				width: 50%!important;
			}

            body .tagline-wrapper{
                left: 200px;
            }
		}


	</style>

</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
        
        <!-- Your site title as branding in the menu -->
        <?php if ( ! has_custom_logo() ) { ?>
            <div class="container">
                <a class="hh_logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><!-- <img src="/wp-content/uploads/2018/09/hh_logo.png" /> --> <img class="head-logo" src="/wp-content/themes/hastehustle/img/hh_logo_meridian2.png" /></a>
                <div class="tagline-wrapper">
                    <img class="tagline" src="/wp-content/uploads/2019/01/hh_tagline.png" />
                </div>
                <div class="social-wrapper">
                    <a href="https://www.facebook.com/hasteandhustle/"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/HasteandHustle"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/hasteandhustle/"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/haste-and-hustle/"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.youtube.com/channel/UCu8Wi_VxAgf7AX_FzKimc9w?view_as=subscriber"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <div class="search-form-wrapper d-none">
               <div class="container">
                   <div class="row">
                       <div class="col-12">
                           <form id="nav-search-form" role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                                <label>
                                    <input type="search" class="search-field"
                                        value="<?php echo get_search_query() ?>" name="s"
                                        title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                                </label>
                                <input type="hidden" name="post_type" value="post" />
                            </form>
                       </div>
                   </div>
               </div>
            </div>
        <?php } else {
            the_custom_logo();
        } ?><!-- end custom logo -->
		<nav class="navbar navbar-expand-lg navbar-dark">

		<?php if ( 'container' == $container ) : ?>
			<div class="container" >
		<?php endif; ?>

					

				<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
				<div class="social-wrapper-nav">
                    <a href="https://www.facebook.com/hasteandhustle/"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/HasteandHustle"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/hasteandhustle/"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/haste-and-hustle/"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.youtube.com/channel/UCu8Wi_VxAgf7AX_FzKimc9w?view_as=subscriber"><i class="fa fa-youtube"></i></a>
                </div>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
