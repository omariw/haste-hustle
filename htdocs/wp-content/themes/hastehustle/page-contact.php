<?php
/**
 * Template Name: Contact Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$upload_dir   = wp_upload_dir();
?>

<div class="wrapper contact-wrapper" id="page-wrapper">
   <div class="contact-hero-wrapper" style="background:url('<?= $upload_dir['baseurl']; ?>/2018/11/about-hero.jpg') no-repeat; background-size: cover;">
        <div class="container" id="content">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="hero-title" >
                        <h1>Contact Us</h1>
                    </div> 
                </div>
                <div class="col-md-8 col-lg-6 d-flex align-items-center">
                        <p class="contact-contribute">Would you like to contribute to the efforts of Haste & Hustle? Well you can, just click the "Contribute!" button and help us keep the entrepreneur community connected!</p>
                </div>
                <div class="col-md-4 col-lg-2 d-flex align-items-center">
                        <a href="/contribute" class="btn btn-primary btn-block contribute-btn">Contribute!</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container contact-info-wrapper">
		<div class="contact-info">
            <div class="row">
               <div class="col-md-4">
                    <h2>Haste & Hustle</h2>
                    <p>Do you have questions and want them answered immediately? Please fill out our contact form and we'll get back to you as soon as we can!</p>
                    <!--<br>
                    <br>
                    <h2>Contribute</h2>
                    <p>Would you like to contribute to the efforts of Haste & Hustle? Well you can, just click the button below and help us keep the entrepreneur community connected!</p>
                    <a href="/contribute" class="btn btn-primary btn-block contribute-btn">Contribute!</a>-->
                </div>
                <div class="col-md-8">
                    <div class="event-form">
                        <?php gravity_form( 4, false, false, false, '', true ); ?>
                    </div>
                </div>
            </div><!-- .row -->
		</div>
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
