<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$category   = get_the_category();
$upload_dir = wp_upload_dir();
if($category) {
    $category_ids = array();
    foreach ($category as $key => $value) {
        if (strpos($value->slug, 'main-hero') !== false || strpos($value->slug, 'secondary-hero') !== false) {
            unset($category[$key]);
        } else {
            $category_name	= $category[$key]->name;
            $category_ids[] = $category[$key]->term_id;
            break;
        }
    }
}

$subtitle           = get_field( "subtitle" );
$first_content      = get_field( "first_content" );
$bio_content        = get_the_content();
$second_content     = get_field( "second_content" );
$third_content      = get_field( "third_content" );
$callout_choice     = get_field( "break_callout_choice" );
$callout_img        = get_field( "break_callout_img" );
$callout_cont       = get_field( "break_callout_content" );
$blockquote_choice  = get_field( "use_block_quote" );
$quote_content      = get_field( "block_quote" );
$blockquote_content = get_field( "content_block_quote" );
$imgblock_choice    = get_field( "use_image_block" );
$img_content        = get_field( "content_image_block" );
$img_for_block      = get_field( "image_for_block" );
$img_descript       = get_field( "img_descript" );
$author_first       = get_the_author_meta('user_firstname');
$author_last        = get_the_author_meta('user_lastname');
$author_bio         = get_the_author_meta('user_description');
$author_avatar      = get_wp_user_avatar(get_the_author_meta('ID'), 'rounded-circle main-avatar', 'original');
$post_date          = get_the_date( 'F j, Y' );
$speaker_quote      = get_field( "favourite_quote" );
if($category_name == 'Speaker') {
    $post_thumbnail = $upload_dir['baseurl'].'/2018/09/sample-hero@2x.jpg';
} else {
    $post_thumbnail = get_the_post_thumbnail_url( $post->ID, 'full' );
}


?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
        <div class="single-hero-image" style="background: url('<?php echo $post_thumbnail; ?>') center no-repeat; background-size: cover;">
        
        </div>
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-sm-12 single-post-content">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1">
                        <header class="entry-header">
                            <?php 
                                if ($category_name == 'Speaker') {
                                    $lead_title = 'Profile';
                                } else {
                                    $lead_title = $category_name;
                                }
                            ?>
                            <div class="row">
                                <?php 
                                    if ($category_name == 'Speaker') { ?>
                                        <div class="col-md-6 speaker-titles">
                                            <h3 class="single-cat-name"><?= $lead_title; ?></h3>
                                            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                            <h2 class="speaker-position"><?php the_field('position_titles'); ?></h2>
                                            
                                            <?php
                                                // check if the repeater field has rows of data
                                                if( have_rows('social_media_links') ): ?>
                                                    <div class="social-media-wrapper">
                                            <?php   // loop through the rows of data
                                                    while ( have_rows('social_media_links') ) : the_row(); ?>

                                                       <p class="social-media-link"><span><?php the_sub_field('social_media_name');?>:</span> <?php the_sub_field('social_media_link');?></p>
                                            <?php
                                                    endwhile; ?>
                                                    </div>
                                            <?php
                                                endif;
                                            ?>
                                            
                                            <?php
                                                // check if the repeater field has rows of data
                                                //if( have_rows('personal_info') ): ?>
                                                    <!--<div class="personal-info-wrapper"> -->
                                            <?php   // loop through the rows of data
                                                   // while ( have_rows('personal_info') ) : the_row(); ?>

                                                       <!--<p class="personal-info-item">
                                                       <span><?php //the_sub_field('personal_info_subject_title');?>:</span> 
                                                       <?php //the_sub_field('personal_information_details');?></p>
                                            <?php
                                                    //endwhile; ?>
                                                    </div>-->
                                            <?php
                                                // endif;
                                            ?>
                                        </div>
                                        <div class="col-md-6 speaker-profile">
                                            <?php echo the_post_thumbnail( $post->ID, 'full' ); ?>
                                        </div>
                                <?php } else { ?>
                                        <div class="col-md-12">
                                            <h3 class="single-cat-name"><?= $lead_title; ?></h3>
                                            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                            <h2 class="single-subtitle"><?= $subtitle; ?></h2>
                                        </div>
                                <?php } ?>
                            </div>
                            
                        </header><!-- .entry-header -->
                        
                        <?php 
                                    if ($category_name == 'Speaker') { ?>
                                        <hr class="speaker-separator">
                        <?php       } ?>
                        <?php if($category_name != 'Speaker') { ?>
                            <div class="single-meta">
                                <div class="row">
                                    <div class="col-md-5 col-sm-12 author-info">
                                        <?= $author_avatar; ?>
                                        <div class="author-details">
                                            <p class="author_name"><?= the_author_posts_link(); ?></p>
                                            <p class="author_bio"><?= $author_bio; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 posted-date">
                                        <p><?= $post_date; ?></p>
                                    </div>
                                    <div class="col-md-3 col-sm-6 share-btn">
                                        <p><a href="#">Share</a></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <div class="first-content">

                            <?= $first_content; ?>
                            
                            <?php if($category_name == 'Speaker') { ?>
                                </div></div>
                                <div class="row blockquote-column speaker-fav-column">
                                    <?php 
                                        if($speaker_quote) { ?>
                                            <div class="col-lg-3 offset-lg-1 col-md-8 offset-md-2 col-sm-8 offset-sm-2 block-quote">
                                                <h4 class="fav-quote">Favourite Quote</h4>
                                                <p><?= $speaker_quote; ?></p>
                                            </div>
                                    <?php } ?>
                                    <div class="<?php if(!$speaker_quote) { echo "offset-md-2 col-md-8 "; } else { echo "col-lg-6 offset-lg-0 col-md-8"; } ?> offset-md-2 col-sm-10 offset-sm-1 quote-content">
                                        <?= $bio_content; ?>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-10 offset-md-1 col-sm-12 after-callout-section">
                                <div class="row">
                                <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1">
                            <?php } ?>
 
                        </div><!-- first-content -->
                        
                        <?php 
                            if ($blockquote_choice ) { ?>
                            </div></div>
                            <div class="row blockquote-column">
                                <div class="col-lg-3 offset-lg-1 col-sm-8 offset-sm-2 block-quote">
                                    <p><?= $quote_content; ?></p>
                                </div>
                                <div class="col-lg-6 offset-lg-0 col-sm-10 offset-sm-1 quote-content">
                                    <?= $blockquote_content; ?>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 after-blockquote-content">
                        <?php
                            }
                        ?>
                        
                        <?php 
                            if ($imgblock_choice) { ?>
                            </div></div>
                            <div class="row imgblock-column">
                                <div class="col-lg-6 offset-lg-2 col-sm-10 offset-sm-1 imgblock-content">
                                    <?= $img_content; ?>
                                </div>
                                <div class="col-lg-4 offset-lg-0 col-sm-10 offset-sm-1 image-section">
                                    <img src="<?= $img_for_block; ?>" />
                                    <p><?= $img_descript; ?></p>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 after-imgblock-content">
                        <?php
                            }
                        ?>
                        <div class="second-content">

                            <?= $second_content; ?>

                        </div><!-- second-content -->
                        <?php 
                            if ($callout_choice) { ?>
                            </div></div></div></div>
                            <div class="row single-callout" style="background:url('<?= $callout_img; ?>') no-repeat; background-size: cover;">
                                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 offset-sm-0">
                                    <?= $callout_cont; ?>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-10 offset-md-1 col-sm-12 after-callout-section">
                            <div class="row">
                            <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1">
                        <?php
                            }
                        ?>
                        
                        <div class="third-content">

                            <?= $third_content; ?>

                        </div><!-- second-content -->
                        
                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( (comments_open() || get_comments_number()) && $category_name != 'Speaker' ) :
                            comments_template();
                        endif;
                        ?>
                        
                        
                    </div>
                </div>
            </div>
        </div> <!-- End of first row -->
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <footer class="entry-footer">
                    <div class="row single-footer-ads">
                        <div class="col-md-4 single-ad">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/ad-example.jpg" />
                        </div>
                        <div class="col-md-4 single-ad">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/ad-example.jpg" />
                        </div>
                        <div class="col-md-4 single-ad">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/ad-example.jpg" />
                        </div>
                    </div>
                    
                    <div class="latest-title" style="background: url('<?php bloginfo('stylesheet_directory'); ?>/img/lastest-new-bkg.jpg') no-repeat; background-size: cover;">
                        <h3>Related Posts</h3>
                    </div>
                    <div class="row latest-posts">
                        <?php
                            $args = array(
                                'category__in' => $category_ids,
                                'category__not_in' => array(126, 27),
                                'post__not_in' => array($post->ID),
                                'posts_per_page'=> 3, // Number of related posts that will be shown.
                                'ignore_sticky_posts'=>1
                            );
                        
                            $related_posts = new WP_Query( $args );
                            
                            while ( $related_posts->have_posts() ) : $related_posts->the_post();
                                $related_the_permalink   = get_the_permalink($post->ID);
                                $related_author_link     = get_author_posts_url($post->post_author);
                                $related_post_date       = get_the_date( 'F j, Y', $post->ID );
                                $related_author_avatar   = get_wp_user_avatar($post->post_author, 'rounded-circle main-avatar', 'original');
                                $related_author_first    = get_the_author_meta('user_firstname');
                                $related_author_last     = get_the_author_meta('user_lastname');

                        ?>
                                <div class="col-lg-4 col-md-12 latest-post">
                                    <?php echo get_the_post_thumbnail($post->ID, array( 400, 400)); ?>
                                    <div class="row post-info">
                                        <div class="feed-post-title col-md-12">
                                            <h1><a href="<?= $related_the_permalink; ?>"><?php the_title(); ?></a></h2>
                                            <hr>
                                        </div>

                                        <div class="feed-author-info col-md-12">
                                            <div class="row">
                                                <div class="d-flex align-items-center col-2 avatar">
                                                    <?= $related_author_avatar; ?>
                                                </div>
                                                <div class="d-flex align-items-center col-5 author_name">
                                                    <p><a href="<?= $related_author_link; ?>"><?= $related_author_first." ".$related_author_last; ?></a></p>
                                                </div>
                                                <div class="d-flex align-items-center col-5 recent_posted_date">
                                                    <p><?= $related_post_date; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            endwhile;
                            wp_reset_query();
                        ?>
                    </div>
                    <div class="latest-title" style="background: url('<?php bloginfo('stylesheet_directory'); ?>/img/lastest-new-bkg.jpg') no-repeat; background-size: cover;">
                        <h3>The Latest</h3>
                    </div>
                    <div class="row latest-posts">
                        <?php
                            $args = array( 'numberposts' => '3', 'exclude' => $post->ID, 'category__not_in' => array(126, 27) );
                            $recent_posts = wp_get_recent_posts( $args );
                            
                            //print_r($recent_posts);
                        ?>
                        
                        <?php
                            if($recent_posts) {
                                foreach( $recent_posts as $recent ) { 
                                    $recent_the_permalink   = get_the_permalink($recent["ID"]);
                                    $recent_title           = $recent["post_title"];
                                    $recent_author_link     = get_author_posts_url($recent["post_author"]);
                                    $recent_post_date       = get_the_date( 'F j, Y', $recent["ID"] );
                                    $recent_author_avatar   = get_wp_user_avatar($recent["post_author"], 'rounded-circle main-avatar', 'original');
                                    $recent_author_first    = get_the_author_meta('user_firstname', $recent["post_author"]);
                                    $recent_author_last     = get_the_author_meta('user_lastname', $recent["post_author"]);

                            ?>
                                    <div class="col-lg-4 col-md-12 latest-post">
                                        <?php echo get_the_post_thumbnail($recent["ID"], array( 400, 400)); ?>
                                        <div class="row post-info">
                                            <div class="feed-post-title col-md-12">
                                                <h1><a href="<?= $recent_the_permalink; ?>"><?= $recent_title; ?></a></h2>
                                                <hr>
                                            </div>

                                            <div class="feed-author-info col-md-12">
                                                <div class="row">
                                                    <div class="d-flex align-items-center col-2 avatar">
                                                       <?= $recent_author_avatar; ?>
                                                    </div>
                                                    <div class="d-flex align-items-center col-5 author_name">
                                                        <p><a href="<?= $recent_author_link; ?>"><?= $recent_author_first." ".$recent_author_last; ?></a></p>
                                                    </div>
                                                    <div class="d-flex align-items-center col-5 recent_posted_date">
                                                        <p><?= $recent_post_date; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            }
                            wp_reset_query();
                        ?>
                    </div>
                </footer><!-- .entry-footer -->
            </div>
        </div> <!-- End of second row -->
    </div>
</article><!-- #post-## -->
<?php
/*wp_link_pages( array(
    'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
    'after'  => '</div>',
) );*/

//understrap_entry_footer(); ?>