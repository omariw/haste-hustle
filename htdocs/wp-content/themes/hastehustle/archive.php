<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$category_title = single_term_title("", false);
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row archive-post-main">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
					    <h1><?= $category_title; ?></h1>
						<?php
						  the_archive_description( '<div class="taxonomy-description">', '</div>' );
						?>
					</header><!-- .page-header -->

					<?php /* Start the Loop */ ?>
					<div class="row archive-post-row">
                        <?php 
                            $numOfCols = 3;
                            $rowCount = 0;
                            while ( have_posts() ) : the_post(); ?>

                                <?php
                                    get_template_part( 'loop-templates/content', 'archive' );
                                    $rowCount++;
                                    if($rowCount % $numOfCols == 0) echo '</div><div class="row archive-post-row">';
                                ?>

                            <?php endwhile; ?>

                        <?php else : ?>

                            <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                        <?php endif; ?>
                    </div>
			</main><!-- #main -->

			<!-- The pagination component -->
            <div class="row">
                <div class="col-md-12">
                    <?php
                        if (function_exists("pagination")) {
                          pagination($query->max_num_pages);
                        }
                    ?>

                    <?php wp_reset_postdata(); ?>
                </div>
            </div>

		<!-- Do the right sidebar check -->
		<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
