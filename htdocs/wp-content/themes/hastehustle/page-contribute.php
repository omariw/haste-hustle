<?php
/**
 * Template Name: Contribute Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$upload_dir   = wp_upload_dir();
?>

<div class="wrapper contribute-wrapper" id="page-wrapper">
    <div class="container contribute-info">
        <div id="contribute-forms" class="row contribute-header">
            <div class="col-md-12 col-lg-8">
                <header class="page-header">
                    <h1><?= the_title(); ?></h1>
                    <p>Would you like to contribute to the efforts of Haste & Hustle? Well you can, just submit your information to the relevant section below and help us keep the entrepreneur community stay connected!</p>
                </header><!-- .page-header -->
            </div>
		</div><!-- .row -->
		<div class="contributions-wrapper">
            <div class="row">
                <div class="contribute-events col-lg-4 col-md-12">
                    <h2>Event Submission</h2>
                    <p>Have an entrepreneurial event you would like to add to Haste & Hustle' event calendar? Submit your event below!</p>
                    <div class="event-form">
                        <button type="button" class="btn btn-primary btn-block contribute-btn" data-toggle="modal" data-target="#eventSubmission">
                            Submit Event
                        </button>
                       <div class="modal" tabindex="-1" role="dialog" id="eventSubmission" aria-labelledby="eventSubmissionLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Event Submission</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <?php gravity_form( 1, false, false, false, '', true ); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            
               <div class="contribute-authors col-lg-4 col-md-12">
                    <h2>Author Profile Submission</h2>
                    <p>Would you like to regularly submit entrepreneur related articles / blogs to our site? Submit your application below!</p>
                    <div class="event-form">
                        <button type="button" class="btn btn-primary btn-block contribute-btn" data-toggle="modal" data-target="#authorSubmission">
                            Submit Author
                        </button>
                       <div class="modal" tabindex="-1" role="dialog" id="authorSubmission" aria-labelledby="authorSubmissionLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Event Submission</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <?php gravity_form( 2, false, false, false, '', true ); ?>
                              </div>
                            </div>
                          </div>
                        </div> 
                    </div>
                </div>
            
                <div class="contribute-article col-lg-4 col-md-12">
                    <h2>Article / Blog Submission</h2>
                    <p>Would you like to a submit entrepreneur related article / blog to our site? Submit your content below and see it on our site!</p>
                    <div class="event-form">
                        <button type="button" class="btn btn-primary btn-block contribute-btn" data-toggle="modal" data-target="#articleSubmission">
                            Submit Article
                        </button>
                       <div class="modal" tabindex="-1" role="dialog" id="articleSubmission" aria-labelledby="articleSubmissionLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Article / Blog Submission</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <?php gravity_form( 3, false, false, false, '', true ); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div><!-- .row -->
		</div>
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
