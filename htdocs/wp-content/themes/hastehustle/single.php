<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'loop-templates/content', 'single' ); ?>

            <?php //understrap_post_nav(); ?>


    <?php endwhile; // end of the loop. ?>


</div><!-- Wrapper end -->

<?php get_footer(); ?>
