<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container          = get_theme_mod( 'understrap_container_type' );
$author_id          = get_the_author_meta('ID');
$author_first       = get_the_author_meta('user_firstname');
$author_paragraph   = get_field('author_paragraph', 'user_'.$author_id );
?>


<div class="wrapper" id="author-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<header class="page-header author-header">
                    <div class="row align-items-start">
                        <div class="col-lg-4">
                            <?php
                            $curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
                                $author_name ) : get_userdata( intval( $author ) );
                            ?>

                            <?php if ( function_exists( 'wpsabox_author_box' ) ) echo wpsabox_author_box(); ?>
                        </div>
                        <div class="col-lg-8">
                            <div class="about_author">
                                <h1>About <?= $author_first; ?></h1>
                                <?= $author_paragraph; ?>
                            </div>
                        </div>
					</div>

				</header><!-- .page-header -->

				<div class="row author-post-main">
                    
                    <div class="col-lg-8 order-lg-1 order-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="authors-posts">
                                    <h3><?= $author_first; ?>'s Posts</h3>
                                </div>
                                <div class="row author-post-row">
                                    <!-- The Loop -->
                                    <?php if ( have_posts() ) : ?>
                                        <?php 
                                            $numOfCols = 2;
                                            $rowCount = 0;
                                            while ( have_posts() ) : the_post(); ?>
                                            <?php 
                                                get_template_part( 'loop-templates/content', 'page' ); 
                                                $rowCount++;
                                                if($rowCount % $numOfCols == 0) echo '</div><div class="row author-post-row">'; 
                                            ?>

                                        <?php endwhile; ?>
                                    </div>
                                    <?php else : ?>

                                        <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- The pagination component -->
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                        if (function_exists("pagination")) {
                                          pagination($query->max_num_pages);
                                        }
                                    ?>

                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <!-- End Loop -->
                    </div>
                    <div class="col-md-12 col-lg-4 order-lg-12 order-1">
                        <div class="authors-top-posts-title">
                            <h3><?= $author_first; ?>'s Top Posts</h3>
                        </div>
                        
                        <div class="authors-top-post-feed">
                            <?php get_template_part( 'sidebar-templates/sidebar', 'author' ); ?>
                        </div>
                        <!--<div class="author-ads">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/ad-example.jpg" />
                        </div>-->
                    </div>
				</div>
                
			</main><!-- #main -->
			

		<!-- Do the right sidebar check -->
		<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
