<?php
/**
 * Month View Title Template
 * The title template for the month view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<div class="tribe-events-title-bar">

	<!-- Month Title -->
	<?php do_action( 'tribe_events_before_the_title' ); ?>
	<div class="event-submission-title text-center" style="background:url('<?php bloginfo('stylesheet_directory'); ?>/img/event-header.jpg') no-repeat; background-size: cover;">
        <h1 class="tribe-events-page-title"><?php echo tribe_get_events_title() ?></h1>
        <p>Do you have an upcoming entrepreneurial event? Click the button below to add to our calendar! </p>
        <a href="/contribute#contribute-forms" class="btn btn-primary btn-lg">Submit Event</a>
    </div>
	<?php do_action( 'tribe_events_after_the_title' ); ?>
    
    
</div>
