<?php
/**
 * Template Name: About Page
 *
 * This template is used for the homepage layout
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$upload_dir   = wp_upload_dir();
?>

<div class="wrapper about-wrapper" id="page-wrapper">
    <div class="about-hero-wrapper" style="background:url('<?= $upload_dir['baseurl']; ?>/2018/11/about-hero.jpg') no-repeat; background-size: cover;">
        <div class="container" id="content">

            <div class="row">

                <div class="col-md-12">

                    <div class="hero-title" >
                        <h1>About Us</h1>

                    </div> 

                </div>

            </div>
        </div>
    </div>
    <div class="container about-info-wrapper">
        <div class="row mission-statement align-items-center">
            <div class="col-lg-2">
                <h2 class="vertical-text about-title">Mission</h2>
            </div>
            <div class="col-lg-8">
                <p>To serve and support entrepreneurs</p>
            </div>
		</div><!-- .row -->
	</div><!-- Container end -->
    <div class="about-values-wrapper">
        <div class="container">
            <div class="row core-values align-items-center">
                <div class="col-lg-2 col-md-12">
                    <h2 class="vertical-text about-title">Core Values</h2>
                </div>
                <div class="col-lg-4 col-sm-6 value-points">
                    <p>Quality over quantity</p>
                    <p>Continual improvement</p>
                    <p>Service focused</p>
                    <p>Teachable</p>
                </div>
                <div class="col-lg-4 col-sm-6 value-points">
                    <p>Grace and understanding</p>
                    <p>Innovation</p>
                    <p>Kindness</p>
                    <p>Giving</p>
                </div>
		    </div><!-- .row -->
        </div><!-- Container end -->
    </div>
    <div class="our-team-wrapper">
        <div class="container">
            <div class="row our-team align-items-center display-flex">
                <div class="col-lg-2">
                    <h2 class="vertical-text our-team-title">Our Team</h2>
                </div>
                <div class="col-lg-8 our-team-members">
                    <div class="row advisory-board">
                        <div class="col-lg-12 team-section-title">
                            <h3>Advisory Board</h3>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/john-stix.jpg" />
                            <h4>John Stix, Co-Founder</h4>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/nesh-pillay.jpg" />
                            <h4>Nesh Pillay</h4>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/amy-birks.jpg" />
                            <h4>Amy Birks</h4>
                        </div>
                    </div>
                    <div class="row management">
                        <div class="col-lg-12 team-section-title">
                            <h3>Management</h3>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/shauna-arnott.jpg" />
                            <h4>Shauna Arnott, Founder and CEO</h4>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/scott-barker.jpg" />
                            <h4>Scott Barker, President</h4>
                        </div>
                        <div class="col-lg-4 team-member">
                            <img src="<?= $upload_dir['baseurl']; ?>/our_team/mike-rogachevsky.jpg" />
                            <h4>Mike Rogachevsky, Instagram Creator</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container welcome-letter-wrapper">
        <div class="row welcome-letter">
            <div class="col-lg-8 offset-lg-2">
                <h3>Welcome Letter from Shauna Arnott, Founder.</h3>
                <p class="lead-statement">In 2016 I had a big  idea for my business.  Two days later I saw Gary Vaynerchuk’s video on YouTube that explicitly stated that ‘ideas are useless without execution’ and it hit me like a ton of bricks.</p>
                <p class="lead-statement">So, what was my idea?</p>
                <p>I wanted to build an event following the principles that Gary preaches and provide insight and support to emerging entrepreneurs. Thus, Haste and Hustle was born.</p>
                <p>The first event happened in Niagara, Ontario one year later.  The stage was filled with truly unique messages by some of the pre-eminent voices of our time; Gary Vaynerchuk, Casey Neistat, Manjit Minhas, Jon Dick, Marc Saltzman, Alan Smithson, Erin Bury, Allie Hughes and many more.  I took many risks to make it happen, but in end it came together, and my dreams came to fruition.</p>
            </div>
		</div><!-- .row -->
		<div class="row block-quote-section welcome-letter">
		    <div class="col-lg-3 offset-lg-1 col-sm-10 offset-sm-1 block-quote">
                <p>"truly unique messages by some of the pre-eminent voices of our time"</p>
            </div>
            <div class="col-lg-6 col-md-12 beside-quote">
                <p>The challenges of running and producing an event are intense and I learned many lessons the hard way, but the most rewarding part is developing the community. I had an incredible team and supporters, so the event was wonderful but all along I was taking big financial risks not knowing if they would or wouldn’t work out. I was fairly terrified every single day.  That’s right I said it, fairly terrified.</p>
                <p>However, the key was connecting with truly wonderful people to support my journey. Again community. </p>
            </div>
		</div>
		<div class="row welcome-letter">
            <div class="col-lg-8 offset-lg-2">
                <p>I have come across amazing people who attended my events and told me how many positive experiences they had there. Attendees mentioned how beneficial it was to connect with a like-minded community and that they wished they had more of that. I miss connecting with the Haste and Hustlers in between events too. It fills my heart to know people came and received value.  I want that same value for everyone (including myself) to spark our lives throughout the year and for us not to have to wait for the Haste and Hustle conference.</p>
                
                <p>It also troubled me deeply that some people came to the event and due to some logistical challenges did not have the experience promised.  We’re working on a solution to this because this community is more important to me than you can imagine and I want to take ownership of those experiences and correct course.  Like many of you I’m an entrepreneur just working to constantly improve.</p>
                    
                <p>So, I am working on a way for us to stay connected. It excites me to continue to provide knowledge, guidance, support and insights for emerging entrepreneurs. I want to see all of you succeed.</p>
            </div>
		</div><!-- .row -->
	</div><!-- Container end -->
	<div class="about-footer-wrapper" style="background:url('<?= $upload_dir['baseurl']; ?>/2018/11/about-footer.jpg') no-repeat; background-size: cover;">
        <div class="container">
            <div class="row about-footer">
                <div class="col-lg-9 offset-lg-1">
                    <p>There is something super special about this community and I invite each of you to join our tribe and stay connected.</p>
                    <div class="join-guest-list">
                        <a data-sumome-listbuilder-id="2dcab572-18d5-4511-b6fd-16051a23bb64">Join the Guest List</a>
                    </div>
                </div>
		    </div><!-- .row -->
        </div><!-- Container end -->
    </div>
</div><!-- Wrapper end -->

<?php get_footer(); ?>
